﻿/* 
 * XBRLAnalyst
 * https://bitbucket.org/FinDynamics/xbrlanalyst
 * 
 * Copyright 2013, FinDynamics Inc.
 * Licensed under the GPL license:
 * http://www.gnu.org/licenses/gpl.html
 *
 *
 *
 * This software is provided 'as-is', without any expressed or implied
 * warranty. In no event will the authors be held liable for any damages arising from the use of this software.
 *
 */
using ExcelDna.Integration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XBRLAnalyst.Visual_Compoments
{
    public class StartUp : IExcelAddIn
    {
        public void AutoOpen()
        {
        //    try
        //    {
        //        string xllPath = (string)XlCall.Excel(XlCall.xlGetName);
        //        var xlApp = (Microsoft.Office.Interop.Excel.Application)ExcelDnaUtil.Application;
        //        xlApp.AddIns.Add(xllPath, true).Installed = true;
        //    }
        //    catch (Exception ex)
        //    {

        //    }
        }

        public void AutoClose()
        {
        }
    }
}
