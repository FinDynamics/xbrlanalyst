namespace XBRLAnalyst
{
    partial class ConnectionDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ConnectionDialog));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.SaveConnectionCB = new System.Windows.Forms.CheckBox();
            this.ConnectButton = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.PasswordEB = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.UserNameEB = new System.Windows.Forms.TextBox();
            this.progressBar = new System.Windows.Forms.ProgressBar();
            this.CopyrightLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(2, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(150, 150);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 29;
            this.pictureBox1.TabStop = false;
            // 
            // SaveConnectionCB
            // 
            this.SaveConnectionCB.AutoSize = true;
            this.SaveConnectionCB.Location = new System.Drawing.Point(175, 62);
            this.SaveConnectionCB.Name = "SaveConnectionCB";
            this.SaveConnectionCB.Size = new System.Drawing.Size(266, 17);
            this.SaveConnectionCB.TabIndex = 2;
            this.SaveConnectionCB.Text = "Save connection credentials between the sessions";
            this.SaveConnectionCB.UseVisualStyleBackColor = true;
            // 
            // ConnectButton
            // 
            this.ConnectButton.Location = new System.Drawing.Point(241, 97);
            this.ConnectButton.Name = "ConnectButton";
            this.ConnectButton.Size = new System.Drawing.Size(151, 23);
            this.ConnectButton.TabIndex = 3;
            this.ConnectButton.Text = "Connect";
            this.ConnectButton.UseVisualStyleBackColor = true;
            this.ConnectButton.Click += new System.EventHandler(this.ConnectButton_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(172, 39);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 13);
            this.label2.TabIndex = 46;
            this.label2.Text = "Password:";
            // 
            // PasswordEB
            // 
            this.PasswordEB.Location = new System.Drawing.Point(241, 36);
            this.PasswordEB.Name = "PasswordEB";
            this.PasswordEB.Size = new System.Drawing.Size(227, 20);
            this.PasswordEB.TabIndex = 1;
            this.PasswordEB.UseSystemPasswordChar = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(172, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 13);
            this.label1.TabIndex = 42;
            this.label1.Text = "User Name:";
            // 
            // UserNameEB
            // 
            this.UserNameEB.Location = new System.Drawing.Point(241, 10);
            this.UserNameEB.Name = "UserNameEB";
            this.UserNameEB.Size = new System.Drawing.Size(227, 20);
            this.UserNameEB.TabIndex = 0;
            this.UserNameEB.Text = "postgres";
            // 
            // progressBar
            // 
            this.progressBar.Location = new System.Drawing.Point(175, 139);
            this.progressBar.Name = "progressBar";
            this.progressBar.Size = new System.Drawing.Size(293, 23);
            this.progressBar.TabIndex = 47;
            // 
            // CopyrightLabel
            // 
            this.CopyrightLabel.AutoSize = true;
            this.CopyrightLabel.Location = new System.Drawing.Point(308, 173);
            this.CopyrightLabel.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.CopyrightLabel.Name = "CopyrightLabel";
            this.CopyrightLabel.Size = new System.Drawing.Size(176, 13);
            this.CopyrightLabel.TabIndex = 48;
            this.CopyrightLabel.Text = "Copyright � 2013 FinDynamics.com";
            // 
            // ConnectionDialog
            // 
            this.AcceptButton = this.ConnectButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(487, 198);
            this.Controls.Add(this.CopyrightLabel);
            this.Controls.Add(this.progressBar);
            this.Controls.Add(this.SaveConnectionCB);
            this.Controls.Add(this.ConnectButton);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.PasswordEB);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.UserNameEB);
            this.Controls.Add(this.pictureBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ConnectionDialog";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Database Connection";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.ConnectionDialog_FormClosed);
            this.Shown += new System.EventHandler(this.ConnectionDialog_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.CheckBox SaveConnectionCB;
        private System.Windows.Forms.Button ConnectButton;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox PasswordEB;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox UserNameEB;
        private System.Windows.Forms.ProgressBar progressBar;
        private System.Windows.Forms.Label CopyrightLabel;
    }
}