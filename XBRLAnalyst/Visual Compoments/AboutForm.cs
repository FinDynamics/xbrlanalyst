﻿/* 
 * XBRLAnalyst
 * https://bitbucket.org/FinDynamics/xbrlanalyst
 * 
 * Copyright 2013, FinDynamics Inc.
 * Licensed under the GPL license:
 * http://www.gnu.org/licenses/gpl.html
 *
 *
 *
 * This software is provided 'as-is', without any expressed or implied
 * warranty. In no event will the authors be held liable for any damages arising from the use of this software.
 *
 */
using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace XBRLAnalyst
{
    [ComVisible(true)]
    public partial class AboutForm : Form
    {
        public AboutForm()
        {
            InitializeComponent();
            //this.VersionLabel.Text = "Version " + Main.InstalledVersion();
        }

        private void CloseButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void UrlLinkLabel_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start("http://findynamics.com/contact-us");
        }

        private void MailLinkLabel_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            LinkLabel label = (LinkLabel)sender;
            Process.Start("mailto:" + label.Text);
        }

        private void AboutForm_Load(object sender, EventArgs e)
        {

        }

        private void Evaluant_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start("http://www.codeproject.com/Articles/18880/State-of-the-Art-Expression-Evaluation");
        }

        private void Lunen_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start("http://npgsql.projects.pgfoundry.org");
        }

        private void DownloadLinkLabel_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start("http://findynamics.com");
        }

        private void LicenseLinkLabel_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start("http://www.gnu.org/licenses/gpl.html");
        }

        private void ExcelDnaDownloadLinkLabel_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start("http://exceldna.codeplex.com/");
        }

        private void Lumen_LinkClicked_1(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start("http://www.codeproject.com/Articles/9258/A-Fast-CSV-Reader");
        }
        private void Finansu_LinkClicked_1(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start("https://code.google.com/p/finansu/");
        }
        private void Refedit_LinkClicked_1(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start("http://www.codeproject.com/Articles/34425/VS-NET-Excel-Addin-Refedit-Control");
        }

        private void VersionLabel_Click(object sender, EventArgs e)
        {

        }

        private void lnkXBRLUS_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start("http://xbrl.us/research/appdev/Pages/496.aspx");
        }
    }
}