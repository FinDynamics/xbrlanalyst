/* 
 * XBRLAnalyst
 * https://bitbucket.org/FinDynamics/xbrlanalyst
 * 
 * Copyright 2013, FinDynamics Inc.
 * Licensed under the GPL license:
 * http://www.gnu.org/licenses/gpl.html
 *
 *
 *
 * This software is provided 'as-is', without any expressed or implied
 * warranty. In no event will the authors be held liable for any damages arising from the use of this software.
 *
 */
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace XBRLAnalyst
{
    public partial class ConnectionDialog : Form
    {
        public ConnectionDialog()
        {
            InitializeComponent();
        }

        private void HandleAdjustProgress(int progress)
        {
            progressBar.Value = progress;
        }


        private void HandleThreadDone(String error)
        {
            if (error != null && error != "")
                MessageBox.Show(this, "Following errors occurred: " + error, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            DialogResult = DialogResult.OK;
        }

        private void ConnectButton_Click(object sender, EventArgs e)
        {
            this.Enabled = false;

            String host = "public.xbrl.us";
            String port = "5432";
            String database = "edgar_db";
            String username = UserNameEB.Text;
            String password = PasswordEB.Text;

            try
            {
                String error = BusinessLogic.businessLogic.ConnectToDb(host, port, database, username, password);
                if (error == null || error == "")
                {
                    ThreadWorker worker = new ThreadWorker();
                    worker.form = this;
                    worker.threadDone += HandleThreadDone;
                    worker.setProgress += HandleAdjustProgress;
                    new Thread(worker.RunInitialization).Start();
                }
                else
                {
                    MessageBox.Show(this, "Cannot connect to database: " + error, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    this.Enabled = true;
                }
            }
            catch (Exception)
            {
                this.Enabled = true;
                return;
            }
        }

        private void ConnectionDialog_Shown(object sender, EventArgs e)
        {
            try
            {
                RegistryKey key = Registry.CurrentUser.CreateSubKey(@"SOFTWARE\XBRLAnalyst1", RegistryKeyPermissionCheck.ReadSubTree);
                UserNameEB.Text = (String)key.GetValue("Username", "your username");
                PasswordEB.Text = (String)key.GetValue("Password", "your password");
                if ((String)key.GetValue("SaveSettings", "True") == "True")
                    SaveConnectionCB.Checked = true;
                else
                    SaveConnectionCB.Checked = false;
            }
            catch (Exception)
            {
            }
        }

        private void ConnectionDialog_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (SaveConnectionCB.Checked)
            {
                try
                {
                    RegistryKey key = Registry.CurrentUser.CreateSubKey(@"SOFTWARE\XBRLAnalyst1", RegistryKeyPermissionCheck.ReadWriteSubTree);
                    key.SetValue("Username", UserNameEB.Text);
                    key.SetValue("Password", PasswordEB.Text);
                    if (SaveConnectionCB.Checked)
                        key.SetValue("SaveSettings", "True");
                    else
                        key.SetValue("SaveSettings", "False");
                }
                catch (Exception)
                {
                }
            }
        }
    }

    public class ThreadWorker
    {
        public delegate void ThreadDone(String error);
        public delegate void SetProgress(int value);
        public event ThreadDone threadDone;
        public event SetProgress setProgress;
        public ConnectionDialog form;

        public void RaiseSetProgress(int progress)
        {
            if (setProgress != null)
                form.Invoke(setProgress, progress);
        }

        public void RaiseThreadDone(String error)
        {
            if (threadDone != null)
                form.Invoke(threadDone, error);
        }

        public void RunInitialization()
        {
            if (BusinessLogic.businessLogic.tagsProcessor.tagsByTID.Count > 0) // Just reconnecting to DB
            {
                RaiseThreadDone("");
                return;
            }
                
            RaiseSetProgress(25);
            String error = BusinessLogic.businessLogic.tagsProcessor.ReadDataFileWithTagsDefinitions();
            if (error != "")
            {
                RaiseThreadDone(error);
                return;
            }

            RaiseSetProgress(30);
            error = BusinessLogic.businessLogic.tagsProcessor.UpdateQnameIDFromDB();
            if (error != "")
            {
                RaiseThreadDone(error);
                return;
            }

            RaiseSetProgress(50);
            error = BusinessLogic.businessLogic.tagsProcessor.FillFormulaData();
            if (error != "")
            {
                RaiseThreadDone(error);
                return;
            }

            RaiseSetProgress(60);
            BusinessLogic.businessLogic.dataMining.FillUpEntityLists();
            RaiseSetProgress(100);

            RaiseThreadDone(error);
        }
    }
}
