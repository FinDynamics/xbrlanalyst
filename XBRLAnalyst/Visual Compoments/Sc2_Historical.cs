﻿/* 
 * XBRLAnalyst
 * https://bitbucket.org/FinDynamics/xbrlanalyst
 * 
 * Copyright 2013, FinDynamics Inc.
 * Licensed under the GPL license:
 * http://www.gnu.org/licenses/gpl.html
 *
 *
 *
 * This software is provided 'as-is', without any expressed or implied
 * warranty. In no event will the authors be held liable for any damages arising from the use of this software.
 *
 */
using Microsoft.Office.Interop.Excel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace XBRLAnalyst
{
    public partial class Sc2_Historical : Form
    {
        private readonly Microsoft.Office.Interop.Excel.Application _excel;

        public Sc2_Historical(Microsoft.Office.Interop.Excel.Application excel)
        {
            _excel = excel;
            InitializeComponent();
            SelectCompaniesRefedit._Excel = _excel;
            SelectTagsRefedit._Excel = _excel;
        }

        private void Sc2_Historical_Shown(object sender, EventArgs e)
        {
            BusinessLogic.businessLogic.FillUpCompaniesListBySearchParameter(null, null, ListOfCompaniesLB.Items);
            BusinessLogic.businessLogic.FillTagsTree(TagsTree.Nodes);
        }

        private void CompanyNameTB_KeyUp(object sender, KeyEventArgs e)
        {
            BusinessLogic.businessLogic.FillUpCompaniesListBySearchParameter(CompanyNameTB.Text, null, ListOfCompaniesLB.Items);
        }

        private void FinishB_Click(object sender, EventArgs e)
        {
            String error = "";
            // Check if we have everything selected
            if (ListOfCompaniesLB.SelectedItems.Count == 0)
            {
                error += "No companies are selected, please select at least one company first.";
            }
            if (error != "")
            {
                MessageBox.Show(this, error, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            //Check if required special tags are selected; if not, add them to SelectedTags to get their values
            int noMissingTags = BusinessLogic.businessLogic.tagsProcessor.lstSpecialTags.Count;
            foreach (SingleTag st in SelectedTags.Items)
            {
                foreach (SpecialTagTracking spt in BusinessLogic.businessLogic.tagsProcessor.lstSpecialTags)
                    if (st.visualName.Contains(spt.visualName))
                    {
                        spt.isSelected = true;
                        spt.inxSelected = BusinessLogic.businessLogic.tagsProcessor.lstSpecialTags.IndexOf(spt);
                        noMissingTags--;
                    }
                if (noMissingTags == 0)
                    break;
            }
            foreach (SpecialTagTracking spt in BusinessLogic.businessLogic.tagsProcessor.lstSpecialTags)
                if (!spt.isSelected)
                {
                    SelectedTags.Items.Add(BusinessLogic.businessLogic.tagsProcessor.tagsByTID[spt.inxTagsByTID]);
                    spt.inxSelected = SelectedTags.Items.Count - 1;
                }

            try
            {
                BusinessLogic.businessLogic.factData.GetAllFacts(SelectedTags.Items, SelectedCompaniesLB.Items, null);
                // If we didn't bring any data, send a message
                if (BusinessLogic.businessLogic.factData.allFacts.Count == 0)
                {
                    MessageBox.Show(this, "We didn't get any data for specified companies, periods and tags", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                dynamic app = ExcelDna.Integration.ExcelDnaUtil.Application;
                Range activeCell = app.ActiveCell;
                CompanyEntity Company = (CompanyEntity)SelectedCompaniesLB.Items[0];

                //Build fiscal periods 
                List<FiscalPeriod> AllFiscalPeriods=new List<FiscalPeriod>();
                for(int inxYear = 2009;inxYear < 2014;++inxYear)
                {
                    AllFiscalPeriods.Add(new FiscalPeriod(inxYear, "1Q"));
                    AllFiscalPeriods.Add(new FiscalPeriod(inxYear, "2Q"));
                    AllFiscalPeriods.Add(new FiscalPeriod(inxYear, "3Q"));
                    AllFiscalPeriods.Add(new FiscalPeriod(inxYear, "Y"));
                }

                BusinessLogic.businessLogic.OutputHeaderTitle(String.Format("Historical Data for the company:{0} (CIK: {1})",
                    Company.companyName, Company.entity_code),
                    app, activeCell.Row, activeCell.Column, AllFiscalPeriods.Count);
                BusinessLogic.businessLogic.OutputPeriods(AllFiscalPeriods, app, activeCell.Row, activeCell.Column+2);
                BusinessLogic.businessLogic.OutputTags(SelectedTags.Items, app, false, activeCell.Row + 1, activeCell.Column);
                BusinessLogic.businessLogic.OutputValues(SelectedCompaniesLB.Items, app, AllFiscalPeriods);
                //app.Range(activeCell, app.Cells[SelectedCompaniesLB.Items.Count + 2, SelectedTags.Items.Count + 2]).Columns.AutoFit();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            BusinessLogic.businessLogic.tagsProcessor.lstSpecialTags.ForEach(x => x.ClearSelected());
            Close();
        }

        private void SelectAll_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            ListOfCompaniesLB.BeginUpdate();
            for (int i = 0; i < ListOfCompaniesLB.Items.Count; i++)
            {
                ListOfCompaniesLB.SetSelected(i, true);
            }
            ListOfCompaniesLB.EndUpdate();
        }

        private void SelectNone_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            ListOfCompaniesLB.BeginUpdate();
            for (int i = 0; i < ListOfCompaniesLB.Items.Count; i++)
            {
                ListOfCompaniesLB.SetSelected(i, false);
            }
            ListOfCompaniesLB.EndUpdate();
        }

        private void TagsTree_AfterCheck(object sender, TreeViewEventArgs e)
        {
            if (((SingleTag)e.Node.Tag).level == 0)
            {
                foreach (TreeNode node in e.Node.Nodes)
                    node.Checked = e.Node.Checked;
            }
        }

        private void MoveCompanyRight_Click(object sender, EventArgs e)
        {
            ListOfCompaniesLB.BeginUpdate();
            SelectedCompaniesLB.BeginUpdate();
            foreach (int i in ListOfCompaniesLB.SelectedIndices)
            {
                bool found = false;
                foreach (CompanyEntity ce in SelectedCompaniesLB.Items)
                    if (ce == ListOfCompaniesLB.Items[i])
                    {
                        found = true;
                        break;
                    }
                if (!found & SelectedCompaniesLB.Items.Count==0)
                    SelectedCompaniesLB.Items.Add(ListOfCompaniesLB.Items[i]);
            }
            ListOfCompaniesLB.EndUpdate();
            SelectedCompaniesLB.EndUpdate();
        }

        private void MoveCompanyLeft_Click(object sender, EventArgs e)
        {
            ListOfCompaniesLB.BeginUpdate();
            SelectedCompaniesLB.BeginUpdate();
            if (SelectedCompaniesLB.SelectedIndex >= 0)
                SelectedCompaniesLB.Items.RemoveAt(SelectedCompaniesLB.SelectedIndex);
            ListOfCompaniesLB.EndUpdate();
            SelectedCompaniesLB.EndUpdate();
        }

        private void MoveTagToRight_Click(object sender, EventArgs e)
        {
            TagsTree.BeginUpdate();
            SelectedTags.BeginUpdate();
            List<SingleTag> lst = BusinessLogic.businessLogic.GetListOfTagIDsInATree(TagsTree.Nodes);
            foreach (SingleTag st1 in lst)
            {
                bool found = false;
                // If we can find this node in the list already, skip it
                foreach (SingleTag st in SelectedTags.Items)
                    if (st == st1)
                    {
                        found = true;
                        break;
                    }
                if (!found)
                    SelectedTags.Items.Add(st1);
            }
            SelectedTags.EndUpdate();
            TagsTree.EndUpdate();
        }

        private void MoveTagToLeft_Click(object sender, EventArgs e)
        {
            SelectedTags.BeginUpdate();
            if (SelectedTags.SelectedIndex >= 0)
                SelectedTags.Items.RemoveAt(SelectedTags.SelectedIndex);
            SelectedTags.EndUpdate();
        }

        private void CancelButtonB_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void LoadSelectedCompanies_Click(object sender, EventArgs e)
        {
            if (SelectCompaniesRefedit.Text == null || SelectCompaniesRefedit.Text == "")
                return;
            Range range = _excel.Range[SelectCompaniesRefedit.Text];
            //int col = range.Column;
            SelectedCompaniesLB.BeginUpdate();
            foreach (Range c in range)
            {
                //if (c.Column != col)
                //    continue;
                if (c.Value2 == null)
                    continue;
                String code_from_excel = c.Value2.ToString();
                if (code_from_excel == "")
                    continue;
                code_from_excel = code_from_excel.PadLeft(10, '0');

                foreach (CompanyEntity ce in BusinessLogic.businessLogic.dataMining.allCompanies.Values)
                {
                    if (ce.entity_code == code_from_excel)
                    {
                        bool found = false;
                        foreach (CompanyEntity ce1 in SelectedCompaniesLB.Items)
                            if (ce == ce1)
                            {
                                found = true;
                                break;
                            }
                        if (!found & SelectedCompaniesLB.Items.Count == 0)
                            SelectedCompaniesLB.Items.Add(ce);
                        break;
                    }
                }

            }
            SelectedCompaniesLB.EndUpdate();
        }

        private void LoadTags_Click(object sender, EventArgs e)
        {
            if (SelectTagsRefedit.Text == null || SelectTagsRefedit.Text == "")
                return;
            Range range = _excel.Range[SelectTagsRefedit.Text];
            //int row = range.Row;
            SelectedCompaniesLB.BeginUpdate();
            foreach (Range c in range)
            {
                //if (c.Row != row)
                //    continue;
                if (c.Value2 == null)
                    continue;
                var code_from_excel = c.Value2;

                foreach (SingleTag st in BusinessLogic.businessLogic.tagsProcessor.tagsByTID.Values)
                {
                    if (st.level == 0)
                        continue;
                    if (st.tid == code_from_excel)
                    {
                        bool found = false;
                        foreach (SingleTag st1 in SelectedTags.Items)
                            if (st == st1)
                            {
                                found = true;
                                break;
                            }
                        if (!found)
                            SelectedTags.Items.Add(st);
                        break;
                    }
                }

            }
            SelectedCompaniesLB.EndUpdate();
        }

        private void TagsTree_AfterCheck_1(object sender, TreeViewEventArgs e)
        {
            if (((SingleTag)e.Node.Tag).level == 0)
            {
                foreach (TreeNode node in e.Node.Nodes)
                    node.Checked = e.Node.Checked;
            }
        }

        private void CancelButtonB_Click_1(object sender, EventArgs e)
        {
            Close();
        }
    }
}
