﻿namespace XBRLAnalyst
{
    partial class AboutForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ProductNameLabel = new System.Windows.Forms.Label();
            this.VersionLabel = new System.Windows.Forms.Label();
            this.CloseButton = new System.Windows.Forms.Button();
            this.CopyrightLabel = new System.Windows.Forms.Label();
            this.HomepageLinkLabel = new System.Windows.Forms.LinkLabel();
            this.LicenseLabel = new System.Windows.Forms.Label();
            this.LicenseLinkLabel = new System.Windows.Forms.LinkLabel();
            this.DownloadLinkLabel = new System.Windows.Forms.LinkLabel();
            this.groupContactUs = new System.Windows.Forms.GroupBox();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.groupPackages = new System.Windows.Forms.GroupBox();
            this.lnkXBRLUS = new System.Windows.Forms.LinkLabel();
            this.linkLabel2 = new System.Windows.Forms.LinkLabel();
            this.ExcelDnaDownloadLinkLabel = new System.Windows.Forms.LinkLabel();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.NPGSQL = new System.Windows.Forms.LinkLabel();
            this.Lunen = new System.Windows.Forms.LinkLabel();
            this.groupContactUs.SuspendLayout();
            this.groupPackages.SuspendLayout();
            this.SuspendLayout();
            // 
            // ProductNameLabel
            // 
            this.ProductNameLabel.AutoSize = true;
            this.ProductNameLabel.Font = new System.Drawing.Font("Segoe UI", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ProductNameLabel.Location = new System.Drawing.Point(12, 9);
            this.ProductNameLabel.Margin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.ProductNameLabel.Name = "ProductNameLabel";
            this.ProductNameLabel.Size = new System.Drawing.Size(159, 32);
            this.ProductNameLabel.TabIndex = 1;
            this.ProductNameLabel.Text = "XBRLAnalyst";
            // 
            // VersionLabel
            // 
            this.VersionLabel.AutoSize = true;
            this.VersionLabel.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.VersionLabel.Location = new System.Drawing.Point(185, 18);
            this.VersionLabel.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.VersionLabel.Name = "VersionLabel";
            this.VersionLabel.Size = new System.Drawing.Size(91, 21);
            this.VersionLabel.TabIndex = 2;
            this.VersionLabel.Text = "Version: 1.2";
            this.VersionLabel.Click += new System.EventHandler(this.VersionLabel_Click);
            // 
            // CloseButton
            // 
            this.CloseButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.CloseButton.Location = new System.Drawing.Point(189, 424);
            this.CloseButton.Name = "CloseButton";
            this.CloseButton.Size = new System.Drawing.Size(75, 23);
            this.CloseButton.TabIndex = 15;
            this.CloseButton.Text = "Close";
            this.CloseButton.UseVisualStyleBackColor = true;
            this.CloseButton.Click += new System.EventHandler(this.CloseButton_Click);
            // 
            // CopyrightLabel
            // 
            this.CopyrightLabel.AutoSize = true;
            this.CopyrightLabel.Location = new System.Drawing.Point(12, 73);
            this.CopyrightLabel.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.CopyrightLabel.Name = "CopyrightLabel";
            this.CopyrightLabel.Size = new System.Drawing.Size(189, 13);
            this.CopyrightLabel.TabIndex = 4;
            this.CopyrightLabel.Text = "Copyright © 2013 FinDynamics.com";
            // 
            // HomepageLinkLabel
            // 
            this.HomepageLinkLabel.ActiveLinkColor = System.Drawing.Color.Blue;
            this.HomepageLinkLabel.AutoSize = true;
            this.HomepageLinkLabel.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline;
            this.HomepageLinkLabel.Location = new System.Drawing.Point(6, 18);
            this.HomepageLinkLabel.Margin = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.HomepageLinkLabel.Name = "HomepageLinkLabel";
            this.HomepageLinkLabel.Size = new System.Drawing.Size(183, 13);
            this.HomepageLinkLabel.TabIndex = 13;
            this.HomepageLinkLabel.TabStop = true;
            this.HomepageLinkLabel.Text = "http://findynamics.com/contact-us";
            this.HomepageLinkLabel.VisitedLinkColor = System.Drawing.Color.Blue;
            this.HomepageLinkLabel.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.UrlLinkLabel_LinkClicked);
            // 
            // LicenseLabel
            // 
            this.LicenseLabel.AutoSize = true;
            this.LicenseLabel.Location = new System.Drawing.Point(12, 89);
            this.LicenseLabel.Margin = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.LicenseLabel.Name = "LicenseLabel";
            this.LicenseLabel.Size = new System.Drawing.Size(168, 13);
            this.LicenseLabel.TabIndex = 5;
            this.LicenseLabel.Text = "Licensed under the GPL license:";
            // 
            // LicenseLinkLabel
            // 
            this.LicenseLinkLabel.ActiveLinkColor = System.Drawing.Color.Blue;
            this.LicenseLinkLabel.AutoSize = true;
            this.LicenseLinkLabel.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline;
            this.LicenseLinkLabel.Location = new System.Drawing.Point(12, 105);
            this.LicenseLinkLabel.Margin = new System.Windows.Forms.Padding(3, 0, 3, 6);
            this.LicenseLinkLabel.Name = "LicenseLinkLabel";
            this.LicenseLinkLabel.Size = new System.Drawing.Size(203, 13);
            this.LicenseLinkLabel.TabIndex = 6;
            this.LicenseLinkLabel.TabStop = true;
            this.LicenseLinkLabel.Text = "http://www.gnu.org/licenses/gpl.html";
            this.LicenseLinkLabel.VisitedLinkColor = System.Drawing.Color.Blue;
            this.LicenseLinkLabel.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.LicenseLinkLabel_LinkClicked);
            // 
            // DownloadLinkLabel
            // 
            this.DownloadLinkLabel.ActiveLinkColor = System.Drawing.Color.Blue;
            this.DownloadLinkLabel.AutoSize = true;
            this.DownloadLinkLabel.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline;
            this.DownloadLinkLabel.Location = new System.Drawing.Point(12, 43);
            this.DownloadLinkLabel.Margin = new System.Windows.Forms.Padding(3, 6, 3, 0);
            this.DownloadLinkLabel.Name = "DownloadLinkLabel";
            this.DownloadLinkLabel.Size = new System.Drawing.Size(155, 13);
            this.DownloadLinkLabel.TabIndex = 3;
            this.DownloadLinkLabel.TabStop = true;
            this.DownloadLinkLabel.Text = "http://www.findynamics.com";
            this.DownloadLinkLabel.VisitedLinkColor = System.Drawing.Color.Blue;
            this.DownloadLinkLabel.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.DownloadLinkLabel_LinkClicked);
            // 
            // groupContactUs
            // 
            this.groupContactUs.Controls.Add(this.HomepageLinkLabel);
            this.groupContactUs.Location = new System.Drawing.Point(12, 375);
            this.groupContactUs.Name = "groupContactUs";
            this.groupContactUs.Size = new System.Drawing.Size(416, 43);
            this.groupContactUs.TabIndex = 19;
            this.groupContactUs.TabStop = false;
            this.groupContactUs.Text = "Contact us, the developers of this add-in";
            // 
            // richTextBox1
            // 
            this.richTextBox1.Enabled = false;
            this.richTextBox1.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.richTextBox1.Location = new System.Drawing.Point(11, 127);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(416, 74);
            this.richTextBox1.TabIndex = 20;
            this.richTextBox1.Text = "This software is provided \'as-is\', without any expressed or implied\nwarranty. In " +
    "no event will the authors be held liable for any damages arising from the use of" +
    " this software.\n";
            // 
            // groupPackages
            // 
            this.groupPackages.Controls.Add(this.lnkXBRLUS);
            this.groupPackages.Controls.Add(this.linkLabel2);
            this.groupPackages.Controls.Add(this.ExcelDnaDownloadLinkLabel);
            this.groupPackages.Controls.Add(this.linkLabel1);
            this.groupPackages.Controls.Add(this.NPGSQL);
            this.groupPackages.Controls.Add(this.Lunen);
            this.groupPackages.Location = new System.Drawing.Point(12, 207);
            this.groupPackages.Name = "groupPackages";
            this.groupPackages.Size = new System.Drawing.Size(416, 162);
            this.groupPackages.TabIndex = 21;
            this.groupPackages.TabStop = false;
            this.groupPackages.Text = "Utilizes the following third-party packages and databases:";
            // 
            // lnkXBRLUS
            // 
            this.lnkXBRLUS.ActiveLinkColor = System.Drawing.Color.Blue;
            this.lnkXBRLUS.AutoSize = true;
            this.lnkXBRLUS.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline;
            this.lnkXBRLUS.Location = new System.Drawing.Point(15, 18);
            this.lnkXBRLUS.Margin = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.lnkXBRLUS.Name = "lnkXBRLUS";
            this.lnkXBRLUS.Size = new System.Drawing.Size(177, 13);
            this.lnkXBRLUS.TabIndex = 18;
            this.lnkXBRLUS.TabStop = true;
            this.lnkXBRLUS.Text = "XBRL US Database of XBRL filings";
            this.lnkXBRLUS.VisitedLinkColor = System.Drawing.Color.Blue;
            this.lnkXBRLUS.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lnkXBRLUS_LinkClicked);
            // 
            // linkLabel2
            // 
            this.linkLabel2.AutoSize = true;
            this.linkLabel2.Location = new System.Drawing.Point(15, 133);
            this.linkLabel2.Name = "linkLabel2";
            this.linkLabel2.Size = new System.Drawing.Size(332, 13);
            this.linkLabel2.TabIndex = 17;
            this.linkLabel2.TabStop = true;
            this.linkLabel2.Text = "RefEdit Control (licensed under The Code Project Open License)";
            // 
            // ExcelDnaDownloadLinkLabel
            // 
            this.ExcelDnaDownloadLinkLabel.ActiveLinkColor = System.Drawing.Color.Blue;
            this.ExcelDnaDownloadLinkLabel.AutoSize = true;
            this.ExcelDnaDownloadLinkLabel.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline;
            this.ExcelDnaDownloadLinkLabel.Location = new System.Drawing.Point(15, 41);
            this.ExcelDnaDownloadLinkLabel.Margin = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.ExcelDnaDownloadLinkLabel.Name = "ExcelDnaDownloadLinkLabel";
            this.ExcelDnaDownloadLinkLabel.Size = new System.Drawing.Size(308, 13);
            this.ExcelDnaDownloadLinkLabel.TabIndex = 8;
            this.ExcelDnaDownloadLinkLabel.TabStop = true;
            this.ExcelDnaDownloadLinkLabel.Text = "Excel DNA ( Copyright © 2005-2012 Govert van Drimmelen) ";
            this.ExcelDnaDownloadLinkLabel.VisitedLinkColor = System.Drawing.Color.Blue;
            // 
            // linkLabel1
            // 
            this.linkLabel1.AutoSize = true;
            this.linkLabel1.Location = new System.Drawing.Point(15, 110);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(246, 13);
            this.linkLabel1.TabIndex = 17;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "FinAnSu (Copyright © 2012 by Bryan McKelvey)";
            // 
            // NPGSQL
            // 
            this.NPGSQL.AutoSize = true;
            this.NPGSQL.Location = new System.Drawing.Point(15, 87);
            this.NPGSQL.Name = "NPGSQL";
            this.NPGSQL.Size = new System.Drawing.Size(341, 13);
            this.NPGSQL.TabIndex = 17;
            this.NPGSQL.TabStop = true;
            this.NPGSQL.Text = "NPGSQL (Copyright © 2002-2010, The Npgsql Development Team)";
            // 
            // Lunen
            // 
            this.Lunen.AutoSize = true;
            this.Lunen.Location = new System.Drawing.Point(15, 64);
            this.Lunen.Name = "Lunen";
            this.Lunen.Size = new System.Drawing.Size(392, 13);
            this.Lunen.TabIndex = 17;
            this.Lunen.TabStop = true;
            this.Lunen.Text = "Lumen Works CSV parser (Copyright © 2011 Sebastien Lorion - MIT License)";
            // 
            // AboutForm
            // 
            this.AcceptButton = this.CloseButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.CloseButton;
            this.ClientSize = new System.Drawing.Size(439, 459);
            this.Controls.Add(this.groupPackages);
            this.Controls.Add(this.richTextBox1);
            this.Controls.Add(this.ProductNameLabel);
            this.Controls.Add(this.VersionLabel);
            this.Controls.Add(this.CopyrightLabel);
            this.Controls.Add(this.groupContactUs);
            this.Controls.Add(this.DownloadLinkLabel);
            this.Controls.Add(this.LicenseLabel);
            this.Controls.Add(this.CloseButton);
            this.Controls.Add(this.LicenseLinkLabel);
            this.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AboutForm";
            this.ShowInTaskbar = false;
            this.Text = "About XBRLAnalyst";
            this.Load += new System.EventHandler(this.AboutForm_Load);
            this.groupContactUs.ResumeLayout(false);
            this.groupContactUs.PerformLayout();
            this.groupPackages.ResumeLayout(false);
            this.groupPackages.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label ProductNameLabel;
        private System.Windows.Forms.Label VersionLabel;
        private System.Windows.Forms.Button CloseButton;
        private System.Windows.Forms.Label CopyrightLabel;
        private System.Windows.Forms.LinkLabel HomepageLinkLabel;
        private System.Windows.Forms.Label LicenseLabel;
        private System.Windows.Forms.LinkLabel LicenseLinkLabel;
        private System.Windows.Forms.LinkLabel DownloadLinkLabel;
        private System.Windows.Forms.GroupBox groupContactUs;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.GroupBox groupPackages;
        private System.Windows.Forms.LinkLabel lnkXBRLUS;
        private System.Windows.Forms.LinkLabel linkLabel2;
        private System.Windows.Forms.LinkLabel ExcelDnaDownloadLinkLabel;
        private System.Windows.Forms.LinkLabel linkLabel1;
        private System.Windows.Forms.LinkLabel NPGSQL;
        private System.Windows.Forms.LinkLabel Lunen;
    }
}