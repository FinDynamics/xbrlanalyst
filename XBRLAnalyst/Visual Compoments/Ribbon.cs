﻿/* 
 * XBRLAnalyst
 * https://bitbucket.org/FinDynamics/xbrlanalyst
 * 
 * Copyright 2013, FinDynamics Inc.
 * Licensed under the GPL license:
 * http://www.gnu.org/licenses/gpl.html
 *
 *
 *
 * This software is provided 'as-is', without any expressed or implied
 * warranty. In no event will the authors be held liable for any damages arising from the use of this software.
 *
 */
using ExcelDna.Integration.CustomUI;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using Excel = Microsoft.Office.Interop.Excel;

namespace XBRLAnalyst
{
    [ComVisible(true)]
    public class MyRibbon : ExcelDna.Integration.CustomUI.ExcelRibbon
    {
        public override string GetCustomUI(string uiName)
        {
            // Get the XML of RIBBON control
            XmlDocument doc = new XmlDocument();
            doc.Load(Assembly.GetExecutingAssembly().GetManifestResourceStream("XBRLAnalyst.Ribbon.xml"));
            return doc.InnerXml;
        }

        public void ConnectToDB_Click(IRibbonControl control)
        {
            // CHeck if lists are already filled
            NativeWindow parent = new NativeWindow();
            parent.AssignHandle(Process.GetCurrentProcess().MainWindowHandle);

            if (BusinessLogic.businessLogic.dbConnection != null &&
                BusinessLogic.businessLogic.dbConnection.State == System.Data.ConnectionState.Open &&
                BusinessLogic.businessLogic.dataMining.allCompanies.Count > 0 && BusinessLogic.businessLogic.tagsProcessor.tagsByElementID.Count > 0)
                MessageBox.Show(parent, "You are already connected to database.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);            
            //if (BusinessLogic.businessLogic.dataMining.allCompanies.Count > 0 && BusinessLogic.businessLogic.tagsProcessor.tagsByElementID.Count > 0)
            //    MessageBox.Show(parent, "You are already connected to database and all initial data has been downloaded", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);            
            else
                new ConnectionDialog().ShowDialog(parent);
            parent.ReleaseHandle();
        }

        public void EditDefinitions_Click(IRibbonControl control)
        {
            String filename = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + @"\data_definitions.csv";  

            Excel.Application excelApp = new Excel.Application();
            excelApp.Visible = true;

            Excel.Workbooks books = excelApp.Workbooks;

            Excel.Workbook sheet = books.Open(filename);
        }

        public void BuildCompsTable_Click(IRibbonControl control)
        {
            if (BusinessLogic.businessLogic.dbConnection != null)
            {
                NativeWindow parent = new NativeWindow();
                parent.AssignHandle(Process.GetCurrentProcess().MainWindowHandle);
                new Sc1_Comps((Microsoft.Office.Interop.Excel.Application)ExcelDna.Integration.ExcelDnaUtil.Application).Show(parent);
                parent.ReleaseHandle();
            }
            else
            {
                NativeWindow parent = new NativeWindow();
                parent.AssignHandle(Process.GetCurrentProcess().MainWindowHandle);
                if (MessageBox.Show(parent, "You do not have connection to database yet, would you like to connect to database now?", "Error", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    new ConnectionDialog().ShowDialog(parent);
                parent.ReleaseHandle();
            }
        }

        public void DownloadNewApp_Click(IRibbonControl control)
        {
            String filename = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + @"\data_definitions.csv";
            if (File.Exists(filename))
            {
                try
                {
                    NativeWindow parent = new NativeWindow();
                    parent.AssignHandle(Process.GetCurrentProcess().MainWindowHandle);
                    if (MessageBox.Show(parent, "We will have to replace your existing definition with new ones, are you OK?", "Replace definitions", MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes)
                        return;
                    String altFileName = filename + "." + DateTime.Now.ToString("yyyyMMdd");
                    if (File.Exists(altFileName))
                        File.Delete(altFileName);
                    File.Copy(filename, altFileName);
                    File.Delete(filename);
                    WebClient Client = new WebClient();
                    Client.DownloadFile(@"http://findynamics.com/downloads/data_definitions.csv", filename);
                }
                catch (Exception)
                { }
            }
        }

        public void UpdateCompsTable_Click(IRibbonControl control)
        {
            if (BusinessLogic.businessLogic.dbConnection != null)
            {
                NativeWindow parent = new NativeWindow();
                parent.AssignHandle(Process.GetCurrentProcess().MainWindowHandle);
                new Sc1b_UpdateComps((Microsoft.Office.Interop.Excel.Application)ExcelDna.Integration.ExcelDnaUtil.Application).Show(parent);
                parent.ReleaseHandle();
            }
            else
            {
                NativeWindow parent = new NativeWindow();
                parent.AssignHandle(Process.GetCurrentProcess().MainWindowHandle);
                if (MessageBox.Show(parent, "You do not have connection to database yet, would you like to connect to database now?", "Error", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    new ConnectionDialog().ShowDialog(parent);
                parent.ReleaseHandle();
            }
        }

        public void tblHistorical_Click(IRibbonControl control)
        {
            NativeWindow parent = new NativeWindow();
            parent.AssignHandle(Process.GetCurrentProcess().MainWindowHandle);
            if (BusinessLogic.businessLogic.dbConnection != null)
            {
                Sc2_Historical histor = new Sc2_Historical((Microsoft.Office.Interop.Excel.Application)ExcelDna.Integration.ExcelDnaUtil.Application);
                histor.Show(parent);
            }
            else
            {
                if (MessageBox.Show(parent, "You do not have connection to database yet, would you like to connect to database now?", "Error", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    ConnectionDialog connection = new ConnectionDialog();
                    connection.ShowDialog(parent);
                }
            }
            parent.ReleaseHandle();
        }


        public override object LoadImage(string imageId)
        {
            return (Bitmap)XBRLAnalyst.Properties.Resource.ResourceManager.GetObject(imageId);
        }

        public void infAbout_Click(IRibbonControl control)
        {
                AboutForm frmAbout = new AboutForm();
                frmAbout.ShowDialog();
        }

        public void webTutorial_Click(IRibbonControl control)
        {
            WebTutorForm frmWebTutor = new WebTutorForm();
            frmWebTutor.ShowDialog();
        }

    }
}
