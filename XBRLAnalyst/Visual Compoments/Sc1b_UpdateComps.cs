﻿/* 
 * XBRLAnalyst
 * https://bitbucket.org/FinDynamics/xbrlanalyst
 * 
 * Copyright 2013, FinDynamics Inc.
 * Licensed under the GPL license:
 * http://www.gnu.org/licenses/gpl.html
 *
 *
 *
 * This software is provided 'as-is', without any expressed or implied
 * warranty. In no event will the authors be held liable for any damages arising from the use of this software.
 *
 */
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Office.Interop.Excel;

namespace XBRLAnalyst
{
    public partial class Sc1b_UpdateComps : Form
    {
        private readonly Microsoft.Office.Interop.Excel.Application _excel;

        public Sc1b_UpdateComps(Microsoft.Office.Interop.Excel.Application excel)
        {
            _excel = excel;
            InitializeComponent();
            SelectCompsTableRefedit._Excel = _excel;

            QuarterYearCB.SelectedIndex = QuarterYearCB.Items.Count - 2;
            QuarterCB.SelectedIndex = 0;
        }

        private void Sc1b_UpdateComps_Shown(object sender, EventArgs e)
        {
            //BusinessLogic.businessLogic.FillUpCompaniesListBySearchParameter(null, null, ListOfCompaniesLB.Items);
            //BusinessLogic.businessLogic.FillTagsTree(TagsTree.Nodes);

            QuarterCB.SelectedIndex = 0;
            QuarterYearCB.SelectedIndex = QuarterYearCB.Items.Count - 2;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            try
            {
                if (SelectCompsTableRefedit.Text == null || SelectCompsTableRefedit.Text == "")
                    return;

                // Reset all positions back to -1 so that we do not have 
                foreach (CompanyEntity ce in BusinessLogic.businessLogic.dataMining.allCompanies.Values)
                {
                    ce.position = -1;
                }

                foreach (SingleTag st in BusinessLogic.businessLogic.tagsProcessor.tagsByTID.Values)
                {
                    st.position = -1;
                }

                List<CompanyEntity> selectedCompanies = new List<CompanyEntity>();
                List<SingleTag> selectedTags = new List<SingleTag>();

                Range range = _excel.Range[SelectCompsTableRefedit.Text];
                // Load companies
                for (int row = 2; row <= range.Rows.Count; row++)
                {
                    if (range.Cells[row, 1].Value2 == null)
                        continue;
                    String code_from_excel = range.Cells[row, 1].Value2.ToString();
                    if (code_from_excel == "")
                        continue;
                    code_from_excel = code_from_excel.PadLeft(10, '0');
                    foreach (CompanyEntity ce in BusinessLogic.businessLogic.dataMining.allCompanies.Values)
                    {
                        if (ce.entity_code == code_from_excel)
                        {
                            ce.position = range.Cells[row, 1].Row;
                            selectedCompanies.Add(ce);
                            break;                            
                        }
                    }

                }

                // Load tags
                Range cell;
                int code;
                String str_code;
                SingleTag tag;

                for (int col = 2; col <= range.Columns.Count; col++)
                {
                    cell = range.Cells[1, col];
                    if (cell.Value2 == null)
                        continue;
                    str_code = cell.Value2.ToString();
                    if (str_code == "")
                        continue;

                    
                    if (!int.TryParse(str_code, out code))
                        continue;

                    if (BusinessLogic.businessLogic.tagsProcessor.tagsByTID.TryGetValue(code, out tag))
                    {
                        tag.position = range.Cells[1, col].Column;
                        selectedTags.Add(tag);
                    }
                }


                String error = "";
                if (QuarterYearCB.SelectedItem == null)
                {
                    error += " No Year selected, you must select at least one tag.";
                }

                if (QuarterCB.SelectedItem == null)
                {
                    error += "No Period selected, you must select at least one tag.";
                }

                if (error != "")
                {
                    MessageBox.Show(this, error, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                
                List<KeyValuePair<int, String>> years = new List<KeyValuePair<int, String>>();
                years.Add(new KeyValuePair<int, String>(int.Parse((String)QuarterYearCB.SelectedItem), (String)QuarterCB.SelectedItem));
                BusinessLogic.businessLogic.factData.GetAllFacts(selectedTags, selectedCompanies, years);

                dynamic app = ExcelDna.Integration.ExcelDnaUtil.Application;
                Range seedCell = app.Cells[range.Cells[1, 1].row - 2, range.Cells[1, 1].column - 1];
                //Clear all cells values and their notes
                //app.Cells[range.Cells[1, 1].row + 1, range.Cells[1, 1].column + 1].Value2="";
                //range.Cells.Value2 = "";

                BusinessLogic.businessLogic.OutputHeaderTitle(String.Format("Fiscal year:{0}, Period:{1}",years[0].Key,years[0].Value),
                    app, seedCell.Row, seedCell.Column, selectedTags.Count);
                BusinessLogic.businessLogic.OutputCompanies(selectedCompanies, app, false, -1, seedCell.Column);
                BusinessLogic.businessLogic.OutputTags(selectedTags, app, true, seedCell.Row, -1);

                //Clear cell content of all selected cells
                foreach (int row in BusinessLogic.businessLogic.selectedRows)
                    foreach (int col in BusinessLogic.businessLogic.selectedCols)
                        app.Cells[row, col].Value2 = "";

                BusinessLogic.businessLogic.OutputValues(selectedCompanies, app, null);

                BusinessLogic.businessLogic.tagsProcessor.lstSpecialTags.ForEach(x => x.ClearSelected());
                
                Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                BusinessLogic.businessLogic.tagsProcessor.lstSpecialTags.ForEach(x => x.ClearSelected());
                return;
            }

        }
    }
}
