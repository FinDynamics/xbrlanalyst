/* 
 * XBRLAnalyst
 * https://bitbucket.org/FinDynamics/xbrlanalyst
 * 
 * Copyright 2013, FinDynamics Inc.
 * Licensed under the GPL license:
 * http://www.gnu.org/licenses/gpl.html
 *
 *
 *
 * This software is provided 'as-is', without any expressed or implied
 * warranty. In no event will the authors be held liable for any damages arising from the use of this software.
 *
 */
using LumenWorks.Framework.IO.Csv;
using Npgsql;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text.RegularExpressions;


namespace XBRLAnalyst
{
    public class BasicTag
    {
        public String tagName;
        public List<int> elementIDs = new List<int>();
        public Dictionary<int, String> taxonomy = new Dictionary<int, string>(); // Key is elementID

        public BasicTag(String s)
        {
            tagName = s.Trim();
        }
    }

    public class SingleTag
    {
        public bool isVisible = false;
        public int level = 0;
        public int tid = -1;
        public String visualName = null;
        public List<BasicTag> tags = new List<BasicTag>();
        public String formula = null;
        public String noteText = null;
        public List<Object> formulaTagDependencies = null;
        public int position = -1;

        public override String ToString()
        {
            return visualName;
        }

        internal void ReadAltTags(String s)
        {
            String[] arr = s.Split(';');
            foreach (String a in arr)
            {
                tags.Add(new BasicTag(a));
            }
        }

        internal void Read(CsvReader reader)
        {
            if (reader.FieldCount > 0 && reader[0] != "")
                tid = int.Parse(reader[0]);
            if (reader.FieldCount > 1 && reader[1] != "")
            {
                if (reader[1].Substring(0, 1) == ">")
                {
                    visualName = reader[1].Substring(1);
                    level = 0;
                }
                else
                {
                    visualName = reader[1];
                    level = 1;
                }
            }
            if (reader.FieldCount > 2 && reader[2] != "")
                tags.Add(new BasicTag(reader[2]));
            if (reader.FieldCount > 3 && reader[3] != "")
                ReadAltTags(reader[3]);
            if (reader.FieldCount > 4 && reader[4] != "")
                formula = reader[4];
        }
    }

    public class SpecialTagTracking    //Defines special required tags that have Text value and should always be queried from DB
    {
        public int tid = 0;
        public String visualName = null;
        public int inxTagsByTID = 0;
        public bool isSelected = false;
        public int inxSelected = 0;

        public SpecialTagTracking(int tid, String visualName, int inxTagsByTID)
        {
            this.tid = tid;
            this.visualName = visualName;
            this.inxTagsByTID = inxTagsByTID;
        }

        public void ClearSelected()
        {
            this.isSelected = false;
            this.inxSelected = 0;
        }

    }


    public class TagsProcessing
    {
        // Reserved TIDs index that allows adding formula tag which are not excplicitly defined with TIDs
        public int reservedTID0 = 1000000; // Lower boundary of the reserved range of TIDs
        public int reservedTID = 1000000;

        // List of all tags in our vocabulary
        public Dictionary<int, SingleTag> tagsByTID = new Dictionary<int, SingleTag>();
        public Dictionary<int, SingleTag> tagsByElementID = new Dictionary<int, SingleTag>();
        public List<SpecialTagTracking> lstSpecialTags = new List<SpecialTagTracking>();
        public List<SingleTag> tagsSpecial = new List<SingleTag>();
        public int TIDEndOfPeriod, TIDTicker;

        public String ReadDataFileWithTagsDefinitions()
        {
            String error = "";
            String explTagName; // Explicit tag names found in formulas
            Regex rx = new Regex(@"\[([a-zA-Z]+)\]");
            bool foundExplTag;
            CsvReader reader = null;
            try
            {
                String filename = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + @"\data_definitions.csv";                
                if (!File.Exists(filename))
                {
                    
                    Stream stream = Assembly.GetExecutingAssembly().GetManifestResourceStream("XBRLAnalyst.data_definitions.csv");
                    FileStream fs = new FileStream(filename, FileMode.Create);

                    byte[] buffer = new byte[8 * 1024];
                    int len;
                    while ((len = stream.Read(buffer, 0, buffer.Length)) > 0)
                    {
                        fs.Write(buffer, 0, len);
                    }
                    fs.Close();
                    fs.Dispose();
                }
                TextReader tr = new StreamReader(filename);
                reader = new CsvReader(tr, false, ',');
            }
            catch (Exception)
            {
                error = "\"data_definitions.csv\" file is probably open in another application.";
                return error;
            }                
        

            SingleTag tag = null, tmpTag = null;
            while (reader.ReadNextRecord())
            {
                tag = new SingleTag();

                try
                {
                    tag.Read(reader);
                }
                catch (Exception e)
                {
                    error += "Exception parsing data occurred: " + e.Message;
                    continue;
                }

                // If TIDs should be < reservedTID0
                if (tag.tid >= reservedTID0)
                {
                    error = "TIDs in \"data_definitions.csv\" should be < " + reservedTID0.ToString();
                    return error;
                }
                // If we already have this tag ID, skip it
                if (tagsByTID.TryGetValue(tag.tid, out tmpTag))
                {
                    error += String.Format("Tag {0} has the same Tag ID {1} as another tag: {2}.\n", tag.visualName, tag.tid, tmpTag.visualName);
                    continue;
                }
                tagsByTID.Add(tag.tid, tag);

                // If the formula contains explicit tags, add them too
                if (tag.formula != null && tag.formula != "")
                {
                    MatchCollection match = rx.Matches(tag.formula);
                    foreach (Match sm in match)
                    {
                        explTagName = sm.Groups[1].Value;
                        foundExplTag = false;
                        for (int inx = reservedTID0; inx < reservedTID; inx++)
                        {
                            if (tagsByTID[inx].visualName == explTagName)
                            {
                                foundExplTag = true;
                                break;
                            }
                        }
                        if (!foundExplTag)
                        {
                            tmpTag = new SingleTag();
                            tmpTag.level = 1;
                            tmpTag.tid = reservedTID;
                            tmpTag.visualName = explTagName;
                            tmpTag.tags.Add(new BasicTag(explTagName));
                            tagsByTID.Add(reservedTID, tmpTag);
                            reservedTID++;
                        }
                    }
                }
            }

            // Check for special required tags (Ticker and EndofPeriod) and store their TIDs in lstSpecialTags
            foreach (KeyValuePair<int, SingleTag> st in tagsByTID)
            {
                if (st.Value.visualName.Contains("Ticker"))
                {
                    lstSpecialTags.Add(new SpecialTagTracking(st.Value.tid, "Ticker", st.Key));
                    TIDTicker = st.Value.tid;
                }
                if (st.Value.visualName.Contains("End of Period"))
                {
                    lstSpecialTags.Add(new SpecialTagTracking(st.Value.tid, "End of Period", st.Key));
                    TIDEndOfPeriod = lstSpecialTags[lstSpecialTags.Count - 1].tid;
                }
                if (lstSpecialTags.Count==2)
                    break;
            }
            if (lstSpecialTags.Count != 2)
                error += "One of required tags (\"Ticker\", \"End of Period\") is missing in the data_definitions.csv file.";

            foreach (SpecialTagTracking spt in lstSpecialTags)
                tagsSpecial.Add(tagsByTID[spt.inxTagsByTID]);

            foreach (SingleTag st in tagsByTID.Values)
            {
                BuildVisualNotes(st);
            }

            foreach (SingleTag st in tagsByTID.Values)
            {
                RecursiveFormulaProcessing(st);
            }

            return error;
        }

        private void BuildVisualNotes(SingleTag st)
        {
            if (st.formula != null && st.formula != "")
            {
                st.noteText = st.formula;
                String name;
                SingleTag tmpTag;
                Regex rx = new Regex(@"\[([a-z0-9A-Z]+)\]");
                MatchCollection match = rx.Matches(st.formula);
                foreach (Match sm in match)
                {
                    name = sm.Groups[1].Value;
                    int tid = 0;
                    // Else, try to see if the name is numeric, to try to find a tag by TID
                    if (int.TryParse(name, out tid) && tagsByTID.TryGetValue(tid, out tmpTag))
                    {
                        st.noteText = st.noteText.Replace(name, tmpTag.visualName);
                    }
                }
            }
            else
                st.noteText = st.visualName;
        }

        private void RecursiveFormulaProcessing(SingleTag st)
        {
            if (st.formula != null && st.formula != "")
            {
                String name;
                SingleTag tmpTag;
                Regex rx = new Regex(@"\[([a-z0-9A-Z]+)\]");
                MatchCollection match = rx.Matches(st.formula);
                foreach (Match sm in match)
                {
                    name = sm.Groups[1].Value;
                    int tid = 0;
                    // Else, try to see if the name is numeric, to try to find a tag by TID
                    if (int.TryParse(name, out tid) && tagsByTID.TryGetValue(tid, out tmpTag) && tmpTag.formula != null && tmpTag.formula != "")
                    {
                        RecursiveFormulaProcessing(tmpTag);
                        st.formula = st.formula.Replace(String.Format("[{0}]", name), "(" + tmpTag.formula + ")");
                    }
                }
            }
        }

        internal string FindAndAddQname(String local_name, int element_id, String taxonomy)
        {
            string error = "";

            foreach (SingleTag st in tagsByTID.Values)
            {
                if (st.level == 0)
                    continue;
                foreach (BasicTag bt in st.tags)
                    if (bt.tagName == local_name)
                    {
                        try
                        {
                            bt.elementIDs.Add(element_id);
                            bt.taxonomy.Add(element_id, taxonomy);
                            tagsByElementID.Add(element_id, st);
                        }
                        catch (Exception)    // Same tag appears in several places of data_definitions
                        {
                            error = "\n Found doublicates of Tag '" + local_name + "', TID: " + st.tid.ToString() + 
                                " and id: " + element_id.ToString() + ".";
                            return error;
                        }

                    }
            }
            return error;
        }

        public String UpdateQnameIDFromDB()
        {
            String list = "", error="";
            NpgsqlDataReader reader;
            foreach (SingleTag st in tagsByTID.Values)
            {
                if (st.level > 0) // Check that it's not Section title and not Non-XBRL tag
                    foreach (BasicTag bt in st.tags)
                        list += "'" + bt.tagName + "',";
            }
            char[] trimChars = { ',' };
            list = list.Trim(trimChars);
            list = list.Replace(",,", ",");
            try
            {
                String qry = String.Format("SELECT DISTINCT element_id, local_name, taxonomy.name, taxonomy_version.version " +
                    "FROM qname, element, taxonomy, taxonomy_version, namespace " +
                    "WHERE qname.qname_id = element.qname_id AND qname.local_name IN ({0}) AND " +
                    "namespace.taxonomy_version_id = taxonomy_version.taxonomy_version_id AND qname.namespace = namespace.uri AND " +
                    "taxonomy_version.taxonomy_id = taxonomy.taxonomy_id ORDER BY local_name", list);
                NpgsqlCommand command = new NpgsqlCommand(qry, BusinessLogic.businessLogic.dbConnection);
                reader = command.ExecuteReader();
            }
            catch (Exception e)
            {
                error += "Error querying element IDs: " + e.Message;
                return error;
            }

            int element_id = 0;
            String local_name = null, taxonomy=null;
            while (reader.Read())
            {
                try
                {
                    element_id = reader.GetInt32(0);
                }
                catch (Exception)
                {
                    element_id = -1;
                }
                try
                {
                    local_name = reader.GetString(1);
                }
                catch (Exception)
                {
                    local_name = null;
                }
                try
                {
                    taxonomy = reader.GetString(2) + " (" + reader.GetString(3) + ")";
                }
                catch (Exception)
                {
                    taxonomy = null;
                }
                error += FindAndAddQname(local_name, element_id, taxonomy);
            }
            return error;
        }


        public String FillFormulaData()
        {
            string error="";

            try
            {
                foreach (SingleTag st in tagsByTID.Values)
                {
                    // Check if there is a formula and in it any dependant tags
                    //                if (st.tags.Count == 0 && st.formula != null && st.formula != "")

                    if (st.formula != null && st.formula != "")
                    {
                        String name;
                        Regex rx = new Regex(@"\[([a-z0-9A-Z]+)\]");
                        MatchCollection match = rx.Matches(st.formula);
                        foreach (Match sm in match)
                        {
                            name = sm.Groups[1].Value;
                            // Where should we add the tag to
                            SingleTag tmpTag = null;
                            if (st.formulaTagDependencies == null)
                                st.formulaTagDependencies = new List<object>();
                            // We have a few hardcoded parameter names coming from Finansu's Formulas
                            int tid = 0;
                            // Else, try to see if the name is numeric, to try to find a tag by TID
                            if (int.TryParse(name, out tid) && tagsByTID.TryGetValue(tid, out tmpTag))
                                st.formulaTagDependencies.Add(tmpTag);
                            else
                            {
                                for (int inx = reservedTID0; inx < reservedTID; inx++)
                                {
                                    if (tagsByTID[inx].visualName == name)
                                    {
                                        st.formulaTagDependencies.Add(tagsByTID[inx]);
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                error += "(processing formulas): " + ex.Message;
                return error;
            }
            return error;
        }
    }
}