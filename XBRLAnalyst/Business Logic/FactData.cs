/* 
 * XBRLAnalyst
 * https://bitbucket.org/FinDynamics/xbrlanalyst
 * 
 * Copyright 2013, FinDynamics Inc.
 * Licensed under the GPL license:
 * http://www.gnu.org/licenses/gpl.html
 *
 *
 *
 * This software is provided 'as-is', without any expressed or implied
 * warranty. In no event will the authors be held liable for any damages arising from the use of this software.
 *
 */
using Npgsql;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace XBRLAnalyst
{
    public class FiscalPeriod
    {
        public int Year;
        public string Period;
        public int cPosition;
        public FiscalPeriod(int Year, string Period)
        {
            this.Year = Year;
            this.Period = Period;
        }
    }
    public class SingleFact
    {
        public CompanyEntity company;
        public int year;
        public String period;
        public SingleTag tag;
        public String actualSelectedTag = null;
        public int element_id=0;
        public decimal factValue;
        public String strValue;
        public String strUnits;

        public SingleFact(CompanyEntity company, int year, String period, SingleTag tag, decimal factValue, String strValue = "N/A", String strUnits = "N/A")
        {
            this.company = company;
            this.period = period;
            this.year = year;
            this.tag = tag;
            this.factValue = factValue;
            this.strValue = strValue;
            this.strUnits = strUnits;
        }
        public string PeriodToString()
        {
            return this.year.ToString()+this.period;
        }
    }

    public class FactData
    {
        public List<SingleFact> allFacts = new List<SingleFact>();

        internal String JoinAllEntityIDs(IList companies)
        {
            String s = "";
            foreach (CompanyEntity ce in companies)
            {
                s += ce.entity_id + ",";
            }
            s = s.Trim(',');
            return s;
        }

        internal String JoinAllTagIDs(IList tags)
        {
            String s = "";
            foreach (SingleTag ce in tags)
            {
                try
                {
                    if (ce.level == 0)
                        continue;
                    ce.isVisible = true;
                    foreach (BasicTag bt in ce.tags)
                    {
                        if (bt.elementIDs != null && bt.elementIDs.Count > 0)
                            s += String.Join(",", bt.elementIDs) + ",";
                    }
                    if (ce.formulaTagDependencies != null)
                        foreach (object fd in ce.formulaTagDependencies)
                        {
                            if (fd.GetType().ToString() == "XBRLAnalyst.SingleTag")
                            {
                                foreach (BasicTag bt in ((SingleTag)fd).tags)
                                {
                                    if (bt.elementIDs != null && bt.elementIDs.Count > 0)
                                        s += String.Join(",", bt.elementIDs) + ",";
                                }
                            }
                        }
                }
                catch (Exception)
                { }
            }
            s = s.Trim(',');
            s = s.Replace(",,", ",");
            return s;
        }

        public void GetAllFacts(IList tags, IList companies, List<KeyValuePair<int, String>> timePeriods)
        {
            // Reset everything first
            allFacts.Clear();

            String joinedTagIds = JoinAllTagIDs(tags);
            String joinedEntityIDs = JoinAllEntityIDs(companies);
            String joinedYears = "(";
            String qry = null, qrySpecial = null;
            decimal effective_value;
            int entity_id, element_id, fiscal_year;
            String fiscal_period, ticker, fact_value, strUnit;
            SingleFact fact = null;
            CompanyEntity company;
            SingleTag tag;

            // Create joined string of special tags IDs and eliminate those from joinedTagIds
            String joinedSpecialTagIds = JoinAllTagIDs(BusinessLogic.businessLogic.tagsProcessor.tagsSpecial);
            joinedTagIds = "," + joinedTagIds + ",";
            foreach (String str in joinedSpecialTagIds.Split(','))
                joinedTagIds = joinedTagIds.Replace("," + str + ",", ",");
            joinedTagIds = joinedTagIds.TrimStart(',');
            joinedTagIds = joinedTagIds.TrimEnd(',');
            if (joinedSpecialTagIds == "")
                joinedSpecialTagIds = "-1";
            if (joinedTagIds == "")
                joinedTagIds = "-1";


            NpgsqlDataReader reader;

            #region Define query strings for All Main facts according to the type of report: Comps table or Historical
            if (timePeriods == null)
            {
                qry = String.Format("SELECT entity_id, element_id, effective_value, fiscal_year, fiscal_period, uom FROM fact, accession, context WHERE context.context_id = fact.context_id AND accession.accession_id = fact.accession_id AND element_id IN ({0}) AND entity_id IN ({1}) AND specifies_dimensions = 'f' AND restatement_index = 1 AND ultimus_index = 1 AND fiscal_period in ('1Q', '2Q', '3Q', '4Q', 'Y')",
                        joinedTagIds, joinedEntityIDs);
                qrySpecial = String.Format("SELECT entity_id, element_id, fact_value, fiscal_year, fiscal_period FROM fact, accession, context WHERE context.context_id = fact.context_id AND accession.accession_id = fact.accession_id AND element_id IN ({0}) AND entity_id IN ({1}) AND specifies_dimensions = 'f' AND restatement_index = 1 AND ultimus_index = 1 AND fiscal_period in ('1Q', '2Q', '3Q', '4Q', 'Y')",
                        joinedSpecialTagIds, joinedEntityIDs);
            }
            else
            {
                foreach (KeyValuePair<int, String> pair in timePeriods)
                {
                    String period;
                    if (pair.Value == "Annual")
                        period = "Y";
                    else
                        period = pair.Value;
                    joinedYears += String.Format("(fiscal_year = {0} AND fiscal_period = '{1}') OR ", pair.Key, period);
                }
                joinedYears = joinedYears.Substring(0, joinedYears.Length - 4);
                joinedYears += ")";

                qry = String.Format("SELECT entity_id, element_id, effective_value, fiscal_year, fiscal_period, uom FROM fact, accession, context WHERE context.context_id = fact.context_id AND accession.accession_id = fact.accession_id AND {0} AND element_id IN ({1}) AND entity_id IN ({2}) AND specifies_dimensions = 'f' AND restatement_index = 1 AND ultimus_index = 1",
                    joinedYears, joinedTagIds, joinedEntityIDs);
                qrySpecial = String.Format("SELECT entity_id, element_id, fact_value, fiscal_year, fiscal_period FROM fact, accession, context WHERE context.context_id = fact.context_id AND accession.accession_id = fact.accession_id AND {0} AND element_id IN ({1}) AND entity_id IN ({2}) AND specifies_dimensions = 'f' AND restatement_index = 1 AND ultimus_index = 1",
                    joinedYears, joinedSpecialTagIds, joinedEntityIDs);
            }
            #endregion

            #region Query Main Facts
            try
            {
                NpgsqlCommand command = new NpgsqlCommand(qry, BusinessLogic.businessLogic.dbConnection);
                reader = command.ExecuteReader();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                if (ex.Message.Contains("broken"))
                    BusinessLogic.businessLogic.dbConnection.Close();
                return;
            }


            while (reader.Read())
            {
                try
                {
                    entity_id = reader.GetInt32(0);
                }
                catch (Exception)
                {
                    entity_id = int.MinValue;
                }
                try
                {
                    element_id = reader.GetInt32(1);
                }
                catch (Exception)
                {
                    element_id = int.MinValue;
                }
                try
                {
                    effective_value = reader.GetDecimal(2);
                    fact_value = "";
                }
                catch (Exception)
                {
                    effective_value = decimal.MinValue;
                    fact_value = BusinessLogic.businessLogic.FactValueMissing;
                }
                try
                {
                    fiscal_year = reader.GetInt32(3);
                }
                catch (Exception)
                {
                    fiscal_year = int.MinValue;
                }
                try
                {
                    fiscal_period = reader.GetString(4);
                }
                catch (Exception)
                {
                    fiscal_period = null;
                }

                try
                {
                    strUnit = reader.GetString(5);
                }
                catch (Exception)
                {
                    strUnit = BusinessLogic.businessLogic.FactValueMissing;
                }

                if (BusinessLogic.businessLogic.dataMining.allCompanies.TryGetValue(entity_id, out company))
                {
                    if (BusinessLogic.businessLogic.tagsProcessor.tagsByElementID.TryGetValue(element_id, out tag))
                    {
                        fact = new SingleFact(company, fiscal_year, fiscal_period, tag, effective_value, fact_value.ToLower(), strUnit);
                        foreach (BasicTag bt in tag.tags)
                        {
                            if (bt.elementIDs.IndexOf(element_id) > -1)
                            {
                                fact.actualSelectedTag = bt.tagName;
                                fact.element_id = element_id;
                                break;
                            }
                        }
                        allFacts.Add(fact);
                    }
                }
            }
            reader.Close();
            #endregion

            #region Query separately a few special Tags with text values
            try
            {
                NpgsqlCommand command = new NpgsqlCommand(qrySpecial, BusinessLogic.businessLogic.dbConnection);
                reader = command.ExecuteReader();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            while (reader.Read())
            {
                try
                {
                    entity_id = reader.GetInt32(0);
                }
                catch (Exception)
                {
                    entity_id = int.MinValue;
                }
                try
                {
                    element_id = reader.GetInt32(1);
                }
                catch (Exception)
                {
                    element_id = int.MinValue;
                }
                try
                {
                    fact_value = reader.GetString(2);
                }
                catch (Exception)
                {
                    fact_value = BusinessLogic.businessLogic.FactValueMissing;
                }
                try
                {
                    fiscal_year = reader.GetInt32(3);
                }
                catch (Exception)
                {
                    fiscal_year = int.MinValue;
                }
                try
                {
                    fiscal_period = reader.GetString(4);
                }
                catch (Exception)
                {
                    fiscal_period = null;
                }

                if (BusinessLogic.businessLogic.dataMining.allCompanies.TryGetValue(entity_id, out company))
                {
                    if (BusinessLogic.businessLogic.tagsProcessor.tagsByElementID.TryGetValue(element_id, out tag))
                    {
                        fact = new SingleFact(company, fiscal_year, fiscal_period, tag, -1, fact_value);
                        foreach (BasicTag bt in tag.tags)
                        {
                            if (bt.elementIDs.IndexOf(element_id) > -1)
                            {
                                fact.actualSelectedTag = bt.tagName;
                                fact.element_id = element_id;
                                break;
                            }
                        }
                        //if (fact.tag.tid == BusinessLogic.businessLogic.tagsProcessor.TIDEndOfPeriod)
                        //    lstRequery4Period.Remove(company);
                        //if (fact.tag.tid == BusinessLogic.businessLogic.tagsProcessor.TIDTicker)
                        //    lstRequery4Ticker.Remove(company);
                        allFacts.Add(fact);
                    }
                }
            }
            reader.Close();

            // Requery the DB for resolving companies whose EndofPeriod was missing in the instance
            int inxFact;
            SingleTag PeriodTag = null;
            BusinessLogic.businessLogic.tagsProcessor.tagsByTID.TryGetValue(BusinessLogic.businessLogic.tagsProcessor.TIDEndOfPeriod, out PeriodTag); // This tag is always present since it's required -- see in TagProcessing

            try
            {
                if (timePeriods == null)
                {
                    qrySpecial = String.Format("SELECT DISTINCT entity_id, period_end, fiscal_year, fiscal_period FROM fact, accession, context WHERE context.context_id = fact.context_id  AND accession.accession_id = fact.accession_id AND entity_id IN ({0}) AND specifies_dimensions = 'f' AND restatement_index = 1 AND ultimus_index = 1",
                        joinedEntityIDs);
                }
                else
                {
                    qrySpecial = String.Format("SELECT DISTINCT entity_id, period_end, fiscal_year, fiscal_period FROM fact, accession, context WHERE context.context_id = fact.context_id  AND accession.accession_id = fact.accession_id AND {0} AND entity_id IN ({1}) AND specifies_dimensions = 'f' AND restatement_index = 1 AND ultimus_index = 1",
                        joinedYears, joinedEntityIDs);
                }
                NpgsqlCommand command = new NpgsqlCommand(qrySpecial, BusinessLogic.businessLogic.dbConnection);
                reader = command.ExecuteReader();
                BusinessLogic.businessLogic.tagsProcessor.tagsByTID.TryGetValue(BusinessLogic.businessLogic.tagsProcessor.TIDEndOfPeriod, out tag);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            while (reader.Read())
            {
                try
                {
                    entity_id = reader.GetInt32(0);
                }
                catch (Exception)
                {
                    entity_id = int.MinValue;
                }
                try
                {
                    fact_value = reader.GetDate(1).AddDays(-1).ToString(); // Shift one day back due to filings particularities
                    if (fact_value == "") // Skip if still missing
                        continue;
                }
                catch (Exception)
                {
                    continue;
                }
                try
                {
                    fiscal_year = reader.GetInt32(2);
                }
                catch (Exception)
                {
                    fiscal_year = int.MinValue;
                }
                try
                {
                    fiscal_period = reader.GetString(3);
                }
                catch (Exception)
                {
                    fiscal_period = null;
                }

                if (BusinessLogic.businessLogic.dataMining.allCompanies.TryGetValue(entity_id, out company))
                {
                    inxFact = allFacts.FindIndex(x => (x.company == company && x.year == fiscal_year && x.period == fiscal_period && x.tag == PeriodTag));
                    if (inxFact == -1)
                    {
                        fact = new SingleFact(company, fiscal_year, fiscal_period, PeriodTag, -1, fact_value);
                        allFacts.Add(fact);
                    }
                    else
                        allFacts[inxFact].strValue = fact_value;
                }
            }
            reader.Close();


            // Requery the DB for Ticker using entry_url since it is a key parameter that can be missing in XBRL instance
            Dictionary<int, String> Tickers = new Dictionary<int, string>(); // List of Tickers with company_id as Key
            SingleTag TickerTag = null;
            BusinessLogic.businessLogic.tagsProcessor.tagsByTID.TryGetValue(BusinessLogic.businessLogic.tagsProcessor.TIDTicker, out TickerTag); // This tag is always present since it's required -- see in TagProcessing
            String tickerParser = "substring(entry_url from \'http://www.sec.gov/Archives/edgar/data/[0-9]+/[0-9]+/([a-z]+)-[0-9]+\\.xml\')";

            try
            {
                qry = String.Format("SELECT DISTINCT entity_id, " + tickerParser + " FROM accession WHERE entity_id IN ({0}) AND restatement_index = 1 AND period_index = 1",
                        joinedEntityIDs);
                // Could use function "ticker(entity_id)" from XBRL.US database
                NpgsqlCommand command = new NpgsqlCommand(qry, BusinessLogic.businessLogic.dbConnection);
                reader = command.ExecuteReader();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Fun: GetAllFacts; " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            while (reader.Read())
            {
                try
                {
                    entity_id = reader.GetInt32(0);
                }
                catch (Exception)
                {
                    entity_id = -1;
                }
                try
                {
                    ticker = reader.GetString(1);
                }
                catch (Exception)
                {
                    ticker = BusinessLogic.businessLogic.FactValueMissing;
                }
                if (BusinessLogic.businessLogic.dataMining.allCompanies.TryGetValue(entity_id, out company))
                {
                    try
                    {
                        if (timePeriods == null) // Historical
                        {
                            List<SingleFact> difPeriods = allFacts.GroupBy(x => x.PeriodToString()).Select(x => x.First()).ToList();
                            foreach (SingleFact y in difPeriods)
                            {
                                inxFact = allFacts.FindIndex(x => (x.company == company && x.year == y.year && x.period == y.period && x.tag == TickerTag));
                                if (inxFact == -1)
                                {
                                    fact = new SingleFact(company, y.year, y.period, TickerTag, decimal.MinValue, ticker.ToLower());
                                    allFacts.Add(fact);
                                }
                                else
                                    allFacts[inxFact].strValue = ticker.ToLower();
                            }
                        }
                        else    // Comps reports
                        {
                            foreach (KeyValuePair<int, String> pair in timePeriods)
                            {
                                inxFact = allFacts.FindIndex(x => (x.company == company && x.year == pair.Key && x.period == pair.Value && x.tag == TickerTag));
                                if (inxFact == -1)
                                {
                                    if (pair.Value == "Annual")
                                        fact = new SingleFact(company, pair.Key, "Y", TickerTag, -1, ticker.ToLower());
                                    else
                                        fact = new SingleFact(company, pair.Key, pair.Value, TickerTag, -1, ticker.ToLower());
                                    
                                    allFacts.Add(fact);
                                }
                                else
                                    allFacts[inxFact].strValue = ticker.ToLower();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                }
            }
            reader.Close();
            #endregion

            #region Populate remaining selected facts that were not found in the XBRL instance
            bool found = false;
            List<SingleFact> difFact = new List<SingleFact>();
            try
            {
                if (timePeriods == null) // Historical form
                {
                    difFact = allFacts.GroupBy(x => x.PeriodToString()).Select(x => x.First()).ToList();
                }
                foreach (CompanyEntity ce in companies)
                {
                    foreach (SingleTag st in tags)
                    {
                        found = false;
                        foreach (SingleFact sf in allFacts)
                        {
                            if (sf.tag == st && sf.company == ce)
                            {
                                found = true;
                                break;
                            }
                        }

                        if (!found)
                            if (timePeriods != null) //Comps table form
                            {
                                foreach (KeyValuePair<int, String> y in timePeriods)
                                {
                                    if (y.Value == "Annual")
                                        fact = new SingleFact(ce, y.Key, "Y", st, decimal.MinValue, BusinessLogic.businessLogic.FactValueMissing);
                                    else
                                        fact = new SingleFact(ce, y.Key, y.Value, st, decimal.MinValue, BusinessLogic.businessLogic.FactValueMissing);
                                    allFacts.Add(fact);
                                }
                            }
                            else //Historical data
                            {
                                foreach (SingleFact y in difFact)
                                {
                                    fact = new SingleFact(ce, y.year, y.period, st, decimal.MinValue, BusinessLogic.businessLogic.FactValueMissing);
                                    allFacts.Add(fact);
                                }
                            }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            #endregion
        }
    }
}
