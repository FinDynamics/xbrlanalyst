/* 
 * XBRLAnalyst
 * https://bitbucket.org/FinDynamics/xbrlanalyst
 * 
 * Copyright 2013, FinDynamics Inc.
 * Licensed under the GPL license:
 * http://www.gnu.org/licenses/gpl.html
 *
 *
 *
 * This software is provided 'as-is', without any expressed or implied
 * warranty. In no event will the authors be held liable for any damages arising from the use of this software.
 *
 */
using Microsoft.Office.Interop.Excel;
using Npgsql;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Text.RegularExpressions;


namespace XBRLAnalyst
{
    public class BusinessLogic
    {
        public NpgsqlConnection dbConnection = null;
        public DataMiningClass dataMining = new DataMiningClass();
        public TagsProcessing tagsProcessor = new TagsProcessing();
        public FactData factData = new FactData();
        public List<int> selectedRows = new List<int>();
        public List<int> selectedCols = new List<int>();
        public string FactValueMissing = "N/A";

        public static BusinessLogic businessLogic = new BusinessLogic();

        public String ConnectToDb(String host, String port, String database, String username, String password)
        {
            if (host == null)
                return "Host name must be specified for your database";

            if (port == null)
                return "Port number must be specified for your database";

            if (username == null)
                return "User name must be specified for your database";

            if (password == null)
                return "Password must be specified for your database";

            if (database == null)
                return "Database name must be specified for your database";

            // Now, try to connect to the database
            try
            {
                // PostgeSQL-style connection string
                string connstring = String.Format("Server={0};Port={1};Database={2};" +
                    "User Id={3};Password={4};CommandTimeout=60", host, port, database, username, password);
                // Making connection with Npgsql provider
                dbConnection = new NpgsqlConnection(connstring);
                dbConnection.Open();
                return null;
            }
            catch (Exception ex)
            {
                dbConnection = null;
                String ret = String.Format("Connection to database {0} cannot be established. We received this error: {1}", database, ex.Message);
                return ret;
            }
        }

        public void FillUpIndustriesListBySearchParameter(String search, IList lb)
        {
            lb.Clear();
            if (search == null || search == "")
            {
                try
                {
                    foreach (IndustryCode ic in dataMining.allIndustries)
                        lb.Add(ic);
                }
                catch (Exception)
                { }
            }
            else
            {
                int sic_number;
                if (int.TryParse(search, out sic_number))
                {
                    try
                    {
                        foreach (IndustryCode ic in dataMining.allIndustries)
                        {
                            if (ic.industryCode.ToString().Contains(search))
                                lb.Add(ic);
                        }
                    }
                    catch (Exception)
                    { }
                }
                else
                {
                    try
                    {
                        foreach (IndustryCode ic in dataMining.allIndustries)
                        {
                            if (ic.industryName.ToLower().Contains(search.ToLower()))
                                lb.Add(ic);
                        }
                    }
                    catch (Exception)
                    { }
                }
            }
        }

        public void FillUpCompaniesListBySearchParameter(String search, IndustryCode selectedIndustry, IList lb)
        {
            ICollection<CompanyEntity> list = null;
            lb.Clear();
            try
            {
                if (selectedIndustry == null)
                    list = dataMining.allCompanies.Values;
                else
                    list = selectedIndustry.companiesList;
                if (search == null || search == "")
                {
                    foreach (CompanyEntity ce in list)
                        lb.Add(ce);
                }
                else
                {
                    foreach (CompanyEntity ce in list)
                        if (ce.entity_code.ToString().Contains(search) || ce.companyName.ToLower().Contains(search.ToLower()))
                            lb.Add(ce);
                }
            }
            catch (Exception)
            { }
        }

        public void FillUpCompaniesListBySelectedIndustries(IList ind, IList comp)
        {
            comp.Clear();
            try
            {
                foreach (IndustryCode ic in ind)
                    foreach (CompanyEntity ce in ic.companiesList)
                        comp.Add(ce);
            }
            catch (Exception)
            { }
        }

        public void FillTagsTree(TreeNodeCollection nodes)
        {
            nodes.Clear();
            TreeNode lastNode = null, nd = null;
            foreach (SingleTag st in tagsProcessor.tagsByTID.Values)
            {
                if (st.level == 0)
                {
                    try
                    {
                        lastNode = new TreeNode(st.visualName);
                        lastNode.Tag = st;
                        lastNode.BackColor = System.Drawing.Color.LightBlue;
                        nodes.Add(lastNode);
                    }
                    catch (Exception)
                    { }
                }
                else if (st.tid < tagsProcessor.reservedTID0)
                {
                    try
                    {
                        nd = new TreeNode(st.visualName);
                        nd.Tag = st;
                        lastNode.Nodes.Add(nd);
                    }
                    catch (Exception)
                    { }
                }


            }
        }

        public List<SingleTag> GetListOfTagIDsInATree(System.Windows.Forms.TreeNodeCollection theNodes)
        {
            List<SingleTag> aResult = new List<SingleTag>();

            if (theNodes != null)
            {
                foreach (System.Windows.Forms.TreeNode aNode in theNodes)
                {
                    try
                    {
                        if (aNode.Checked)
                        {
                            if (aNode.Tag != null && ((SingleTag)aNode.Tag).level > 0)
                                aResult.Add((SingleTag)aNode.Tag);
                        }

                        aResult.AddRange(GetListOfTagIDsInATree(aNode.Nodes));
                    }
                    catch (Exception)
                    { }
                }
            }

            return aResult;
        }

        public void OutputHeaderTitle(String s, dynamic app, int row, int col, int ntags)
        {
            Range cell;

            selectedRows.Clear();
            selectedCols.Clear();

            try
            {
                cell = app.Cells[row, col];
                Range rTitle = app.Range(cell, app.Cells[row, col + 1 + ntags]);
                rTitle.Merge(Type.Missing);
                cell.Font.Size = 14;
                cell.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.White);
                rTitle.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);

                if (s.Contains("Historical")) //Historical data form
                {
                    cell.Value = s;
                }
                else
                {
                    cell.Value = "Public Comparables Analysis";

                    cell = app.Cells[row + 1, col];
                    cell.Value = s;
                    cell.Font.Bold = true;
                    cell = app.Cells[row + 2, col + 1];
                    cell.Value = "CIK/TID";
                    cell.Borders.ColorIndex = 1;
                    cell.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.LightGray);
                }
            }
            catch (Exception)
            { }
        }

        public void OutputCompanies(IList selected, dynamic app, bool isHorisontal, int row, int col)
        {
            Range cell;
            // Let's draw a top row (company names)
            foreach (CompanyEntity ce in selected)
            {
                try
                {
                    if (row==-1) // rows provided from Update form by ce.position
                        cell = app.Cells[ce.position, col];
                    else
                        cell = app.Cells[row + 1, col];

                    //cell.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.LightBlue);
                    cell.Value = System.Globalization.CultureInfo.CurrentCulture.TextInfo.ToTitleCase(ce.companyName.ToLower());
                    cell.Font.Italic = true;
                    if (isHorisontal)
                    {
                        // Output CIK as separate (col/row)
                        cell = app.Cells[row + 2, col];
                        cell.NumberFormat = "@";
                        cell.Borders[XlBordersIndex.xlEdgeLeft].ColorIndex = 1;
                        cell.Borders[XlBordersIndex.xlEdgeRight].ColorIndex = 1;
                        cell.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.LightGray);
                        cell.Value = ce.entity_code.PadLeft(10, '0').ToString();
                        ce.position = col;
                        col++;
                    }
                    else
                    {
                        // Output CIK as separate (col/row)
                        if (row == -1) // rows provided from Update form by ce.position
                            cell = app.Cells[ce.position, col + 1];
                        else
                        {
                            cell = app.Cells[row + 1, col + 1];
                            ce.position = row + 1;
                            row++;
                        }
                        selectedRows.Add(ce.position);
                        cell.NumberFormat = "@";
                        cell.Borders[XlBordersIndex.xlEdgeLeft].ColorIndex = 1;
                        cell.Borders[XlBordersIndex.xlEdgeRight].ColorIndex = 1;
                        cell.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.LightGray);
                        cell.Value = ce.entity_code.PadLeft(10, '0').ToString();
                    }
                }
                catch (Exception)
                { }
            }
        }


        public void OutputPeriods(IList selected, dynamic app, int row, int col)
        {
            Range cell;
            foreach (FiscalPeriod cp in selected)
            {
                cell = app.Cells[row + 1, col];
                cell.Value = cp.Year.ToString() + "-" + cp.Period;
                cell.Borders[XlBordersIndex.xlEdgeBottom].ColorIndex = 1;
                cp.cPosition = col;
                selectedCols.Add(cp.cPosition);
                col++;
            }
        }

        public void OutputTags(IList selected, dynamic app, bool isHorisontal, int row, int col)
        {
            List<int> lstIgnore=new List<int>();
            foreach (SpecialTagTracking spt in BusinessLogic.businessLogic.tagsProcessor.lstSpecialTags)
                if (!spt.isSelected)
                    lstIgnore.Add(spt.inxSelected);

            try
            {
                Range cell1, cell2;
                // Let's draw a side column (value names)
                foreach (SingleTag st in selected)
                {
                    if (lstIgnore.IndexOf(selected.IndexOf(st))>-1) // Check if the tag should be ignored, because it's a special required tag that was not selected by user
                        continue;
                    if (st.tid >= BusinessLogic.businessLogic.tagsProcessor.reservedTID0) // Ignore reserved tags that were added for calculations
                        continue;

                    if (isHorisontal) // Comps table form
                    {
                        if (col == -1) // Provided by Update form
                        {
                            //cell1 = app.Cells[row + 1, st.position];
                            cell2 = app.Cells[row + 2, st.position];
                        }
                        else
                        {
                            cell1 = app.Cells[row + 1, col];
                            cell1.Value = st.visualName;
                            cell1.Font.Bold = true;
                            cell1.Borders[XlBordersIndex.xlEdgeBottom].ColorIndex = 1;
                            cell2 = app.Cells[row + 2, col];
                            st.position = col;
                            col++;
                        }
                        selectedCols.Add(st.position);

                        // Output TID as separate (col/row)
                        cell2.Value = st.tid;
                        cell2.Borders[XlBordersIndex.xlEdgeBottom].ColorIndex = 1;
                        cell2.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.LightGray);
                    }
                    else
                    {
                        if (row == -1) // Provided by Update form
                        {
                            cell1 = app.Cells[st.position, col];
                            cell2 = app.Cells[st.position, col + 1];
                        }
                        else
                        {
                            cell1 = app.Cells[row + 1, col];
                            cell2 = app.Cells[row + 1, col+1];
                            st.position = row + 1;
                            row++;
                        }
                        //cell.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.LightCoral);
                        cell1.Value = st.visualName;
                        cell1.Font.Bold = true;

                        // Output TID as separate (col/row)
                        cell2.Value = st.tid;
                        cell2.Borders[XlBordersIndex.xlEdgeLeft].ColorIndex = 1;
                        cell2.Borders[XlBordersIndex.xlEdgeRight].ColorIndex = 1;
                        cell2.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.LightGray);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

        }

        public void OutputValues(IList selected, dynamic app, List<FiscalPeriod> AllFiscalPeriods)
        {
            Range cell;
            string cellFormula, textFormula;

            //Dictionary<int, String> CurrentPrice = new Dictionary<int, String>();
            //Dictionary<int, String> CurrentVolume = new Dictionary<int, String>();
            Dictionary<int, String> SharePrice = new Dictionary<int, String>();
            int inx, inxTag;
            FiscalPeriod fp1;
            bool found;

            foreach (SingleFact sf in BusinessLogic.businessLogic.factData.allFacts)
            {
                try
                {
                    // Special tags are always present in allFacts. Check if it's special that it was selected.
                    if (BusinessLogic.businessLogic.tagsProcessor.lstSpecialTags.FindIndex(x => x.tid == sf.tag.tid && !x.isSelected) > -1)
                        continue;

                    if (sf.tag.isVisible && sf.tag.tid < BusinessLogic.businessLogic.tagsProcessor.reservedTID0)
                    {
                        if ((AllFiscalPeriods != null && sf.tag.position > 0) || (sf.company.position > 0 && sf.tag.position > 0))
                        {
                            #region Set the output cell
                            cell = null;
                            if (AllFiscalPeriods == null) // Comps table
                            {
                                cell = app.Cells[sf.company.position, sf.tag.position];
                                inx = sf.company.position;
                            }
                            else // Historical
                            {
                                fp1 = AllFiscalPeriods.Find(x => (x.Year == sf.year & x.Period == sf.period));
                                if (fp1 == null) // Exclude from historical data (typically corresponds to the year 2008)
                                    continue;
                                cell = app.Cells[sf.tag.position, fp1.cPosition];
                                inx = fp1.cPosition;
                            }
                            if (cell == null)
                                continue;
                            #endregion

                            if (sf.tag != null && sf.tag.tags != null && sf.tag.tags.Count > 0)
                            {
                                // If this is the main tag, always overwrite
                                if (sf.actualSelectedTag == sf.tag.tags[0].tagName)
                                {
                                    if (sf.strValue == "")
                                        cell.Value2 = sf.factValue;
                                    else if (cell.Value2 == null || (cell.Value2).ToString() == FactValueMissing)
                                        cell.Value2 = sf.strValue;
                                    cell.NoteText(String.Format("{0}, tag: {1}\nUnits: {2}", sf.tag.tags[0].taxonomy[sf.element_id], sf.actualSelectedTag, sf.strUnits));
                                    if (sf.strUnits != "USD" && sf.strUnits != BusinessLogic.businessLogic.FactValueMissing)
                                        cell.Font.Color = System.Drawing.Color.Olive;
                                }
                                else
                                {
                                    if (cell.Value2 == null || (cell.Value2).ToString() == FactValueMissing)
                                    {
                                        if (sf.strValue == "")
                                            cell.Value2 = sf.factValue;
                                        else
                                            cell.Value2 = sf.strValue;
                                        if (sf.actualSelectedTag != null)
                                        {
                                            inxTag = sf.tag.tags.FindIndex(x => (x.tagName == sf.actualSelectedTag));
                                            cell.NoteText(String.Format("{0}, tag: {1}\nUnits: {2}", 
                                                sf.tag.tags[inxTag].taxonomy[sf.element_id], sf.actualSelectedTag, sf.strUnits));
                                            if (sf.strUnits != "USD" && sf.strUnits != BusinessLogic.businessLogic.FactValueMissing)
                                                cell.Font.Color = System.Drawing.Color.Olive;
                                        }
                                        else if (sf.tag.tid == BusinessLogic.businessLogic.tagsProcessor.TIDEndOfPeriod)
                                            cell.NoteText("'period_end' context");
                                        else
                                            cell.NoteText(FactValueMissing);
                                    }
                                }
                            }
                            
                            if (sf.tag.formulaTagDependencies != null && (cell.Value2 == null || (cell.Value2).ToString() == FactValueMissing))
                            {
                                textFormula = sf.tag.formula;
                                cellFormula = "=" + textFormula;
                                // Find the value we need for each tag
                                foreach (SingleTag st in sf.tag.formulaTagDependencies)
                                {
                                    // Find what value corresponds to this tag
                                    foreach (SingleFact sf1 in BusinessLogic.businessLogic.factData.allFacts)
                                    {
                                        if (st != sf1.tag || sf.company.companyName != sf1.company.companyName
                                            || sf.year != sf1.year || sf.period != sf1.period)
                                            continue;
                                        // Correct order is important
                                        if (sf1.actualSelectedTag != null)
                                        {
                                            found = false;
                                            for (int i = 0; i < st.tags.Count; i++)
                                            {
                                                if (sf1.actualSelectedTag == st.tags[i].tagName)
                                                {
                                                    found=true;
                                                    break;
                                                }
                                            }
                                        }
                                        else
                                            found=true;
                                        if (found)
                                        {
                                            foreach (BasicTag bt in sf1.tag.tags)
                                            {
                                                if (sf1.strValue == "")
                                                    cellFormula = cellFormula.Replace(String.Format("[{0}]", bt.tagName), sf1.factValue.ToString());
                                                else
                                                    cellFormula = cellFormula.Replace(String.Format("[{0}]", bt.tagName), sf1.strValue);
                                            }
                                            if (sf1.strValue == "")
                                                cellFormula = cellFormula.Replace(String.Format("[{0}]", sf1.tag.tid), sf1.factValue.ToString());
                                            else
                                                cellFormula = cellFormula.Replace(String.Format("[{0}]", sf1.tag.tid), sf1.strValue);
                                        }
                                    }
                                }
                                cellFormula = Regex.Replace(cellFormula, "[[A-Za-z0-9]+]", FactValueMissing);
                                cellFormula = cellFormula.Replace("[", "");
                                cellFormula = cellFormula.Replace("]", "");
                                cell.Value2 = cellFormula;
                                cell.NoteText(sf.tag.noteText);
                                //cell.Comment.Text(sf.tag.noteText);
                                //cell.Comment.Visible = true;
                                //cell.Comment.Shape.TextFrame.AutoSize = true;
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }
        }
    }
}
