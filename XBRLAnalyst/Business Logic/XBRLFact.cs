﻿/* 
 * XBRLAnalyst
 * https://bitbucket.org/FinDynamics/xbrlanalyst
 * 
 * Copyright 2013, FinDynamics Inc.
 * Licensed under the GPL license:
 * http://www.gnu.org/licenses/gpl.html
 *
 *
 *
 * This software is provided 'as-is', without any expressed or implied
 * warranty. In no event will the authors be held liable for any damages arising from the use of this software.
 *
 */
using Microsoft.Office.Interop.Excel;
using System;
using System.Text.RegularExpressions;
using ExcelDna.Integration;
using System.Windows.Forms;
using Npgsql;
using System.Diagnostics;
using System.Collections.Generic;

namespace XBRLAnalyst
{
    public static class XBRLfun
    {
        #region XBRLFact
        /// <summary>
        /// Returns the financial fact from XBRL filing of a public company.
        /// </summary>
        /// <param name="CIK">Company Index Key (CIK)</param>
        /// <param name="Tag or TID">XBRL tag (e.g. SalesRevenueNet) or TID (e.g. [501]), which is defined in data_definitions.csv</param>
        /// <param name="Year">Fiscal year of the filing</param>
        /// <param name="Period">Fiscal period, e.g. "Y" for yearly or "Q1" for first quarter</param>
        /// <param name="Member (Optional)">XBRL Dimension Member, e.g. "PensionPlansDefinedBenefitMember" or "All" for all Members</param>
        /// <returns>Returns the value or NoData if no fact was found</returns>
        [ExcelFunction("Returns the financial fact from XBRL filing of a public company. \n" + 
            "If the optional parameter MEMBER is provided and equal \"All\", XBRLFact becomes an array function, which "
            + "returns count of members in the first line and all the members thereunder.")]
        public static object[,] XBRLFact(
            [ExcelArgument("is the Company Index Key (CIK).", Name = "CIK")] string CIK,
            [ExcelArgument("is the XBRL tag (e.g. SalesRevenueNet) or TID (e.g. [501]), which is defined in data_definitions.csv",
                Name = "Tag or TID")] string Tag,
            [ExcelArgument("is the Fiscal year of the filing, e.g. 2013.", Name = "Year")] string Year,
            [ExcelArgument("is the Fiscal period, e.g. \"Y\" for annual or \"2Q\" for second quarter.",
                Name = "Period")] string Period,
            [ExcelArgument("is an XBRL Dimension Member, e.g. \"PensionPlansDefinedBenefitMember\" or \"All\" for all Members",
                Name = "Member (Optional)")] string Member = "")
        {
            const string NoData="nodata";
            string[,] objNaN = new string[,] { { "NaN" } };
            object[,] result;
            List<object[]> DBRecord = new List<object[]>();
            int entity_id = 0, element_id;
            string qry, fact_value = NoData, listElement_id = "", DomainSegment = "";

            Member = Member.Trim();

            if (CIK == null || CIK == "" || Tag == null || Tag == "" || Year == null || Year == "" || Period == null || Period == "")
                return objNaN;

            // Check connection to DB
            if (BusinessLogic.businessLogic.dbConnection == null ||
                BusinessLogic.businessLogic.dbConnection.State != System.Data.ConnectionState.Open)
                return new object[1, 1] { { "No connection" } };

            //Find company's entity_id by CIK
            CompanyEntity company=null;
            foreach (CompanyEntity ce in BusinessLogic.businessLogic.dataMining.allCompanies.Values)
            {
                if (ce.entity_code == CIK)
                {
                    entity_id = ce.entity_id;
                    company = ce;
                    break;
                }

            }
            if (entity_id == 0) return objNaN;

            NpgsqlDataReader reader;
            NpgsqlCommand command;

            // Find element_id
            bool isTID=Tag.StartsWith("[");
            List<SingleTag> Tags = new List<SingleTag>();
            SingleTag st;
            int TID;
            if (isTID) // This is TID
            {
                if (Member != "") return new object[1, 1] { { "Members not allowed with TID" } };
                TID = System.Convert.ToInt32(Tag.Substring(1, Tag.Length - 2));
                BusinessLogic.businessLogic.tagsProcessor.tagsByTID.TryGetValue(TID, out st);
                Tags.Add(st);
                if (st != null)
                    listElement_id = BusinessLogic.businessLogic.factData.JoinAllTagIDs(Tags);
                else
                    return new object[1,1] {{"TID not found"}};
            }
            else
            {
                TID = -1;
                try
                {
                    qry = String.Format("SELECT element_id FROM element, qname WHERE qname.qname_id = element.qname_id AND qname.local_name='" + Tag + "'");
                    command = new NpgsqlCommand(qry, BusinessLogic.businessLogic.dbConnection);
                    reader = command.ExecuteReader();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return objNaN;
                }

                while (reader.Read())
                {
                    try
                    {
                        element_id = reader.GetInt32(0);
                        listElement_id += element_id.ToString() + ",";
                    }
                    catch (Exception)
                    {
                    }
                }
                listElement_id = listElement_id.TrimEnd(',');
                reader.Close();
            }

            // Read XBRL fact
            decimal effective_value;
            SingleFact fact;
            string strUnit;
            if (listElement_id.Length > 0)
            {
                try
                {
                    // Query the financial fact
                    if (Member == "")
                        qry = String.Format("SELECT element_id, fact_value, effective_value, uom, 'N/A' FROM fact, accession, context WHERE context.context_id = fact.context_id AND accession.accession_id = fact.accession_id AND entity_id={0} AND element_id in ({1}) AND fiscal_year = {2} AND fiscal_period = '{3}' AND specifies_dimensions = 'f' AND restatement_index = 1 AND ultimus_index = 1",
                                entity_id, listElement_id, Year, Period.ToUpper());
                    else
                        qry = String.Format("SELECT element_id, fact_value, effective_value, uom, local_name FROM fact, accession, context, context_dimension, qname WHERE context.context_id = fact.context_id AND accession.accession_id = fact.accession_id AND context.context_id=context_dimension.context_id AND entity_id={0} AND element_id in ({1}) AND fiscal_year = {2} AND fiscal_period = '{3}' AND specifies_dimensions = 't' AND restatement_index = 1 AND ultimus_index = 1 AND qname.qname_id=member_qname_id and is_segment='t'",
                                entity_id, listElement_id, Year, Period.ToUpper());

                    command = new NpgsqlCommand(qry, BusinessLogic.businessLogic.dbConnection);
                    reader = command.ExecuteReader();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return objNaN;
                }

                BusinessLogic.businessLogic.factData.allFacts.Clear();
                int intYear = System.Convert.ToInt32(Year);
                while (reader.Read())
                {
                    try
                    {
                        element_id = reader.GetInt32(0);
                    }
                    catch
                    {
                        continue;
                    }

                    try
                    {
                        fact_value = reader.GetString(1);
                    }
                    catch
                    {
                        fact_value = NoData;
                    }

                    try
                    {
                        effective_value = reader.GetDecimal(2);
                    }
                    catch
                    {
                        effective_value = decimal.MinValue;
                    }

                    try
                    {
                        strUnit = reader.GetString(3);
                    }
                    catch (Exception)
                    {
                        strUnit = BusinessLogic.businessLogic.FactValueMissing;
                    }

                    try
                    {
                        DomainSegment = reader.GetString(4);
                    }
                    catch (Exception)
                    {
                        DomainSegment = NoData;
                    }

                    try
                    {
                        if (isTID)
                        {
                            // Adding the Fact 
                            if (BusinessLogic.businessLogic.tagsProcessor.tagsByElementID.TryGetValue(element_id, out st))
                            {
                                if (effective_value > decimal.MinValue)
                                    fact = new SingleFact(company, intYear, Period, st, effective_value, "", strUnit);
                                else
                                    fact = new SingleFact(company, intYear, Period, st, effective_value, fact_value.ToLower(), strUnit);

                                foreach (BasicTag bt in st.tags)
                                {
                                    if (bt.elementIDs.IndexOf(element_id) > -1)
                                    {
                                        fact.actualSelectedTag = bt.tagName;
                                        fact.element_id = element_id;
                                        break;
                                    }
                                }
                                BusinessLogic.businessLogic.factData.allFacts.Add(fact);
                            }
                        }
                        else
                            if (effective_value > decimal.MinValue)
                                DBRecord.Add(new object[] { (double)effective_value, DomainSegment });
                            else
                                DBRecord.Add(new object[] { fact_value, DomainSegment });
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        reader.Close();
                        return objNaN;
                    }

                    if (DomainSegment != NoData && DomainSegment == Member)
                        break;
                }
                reader.Close();

                //Perform the output after reading DB, because Npgsql does not return the number of records found
                if (isTID)
                {
                    result = new object[1, 3]; // Value, Units, Tag VisualName
                    String textFormula, cellFormula;
                    bool found;

                    #region Populate remaining selected facts that were not found in the XBRL instance
                    foreach (SingleTag st0 in Tags)
                    {
                        found = false;
                        foreach (SingleFact sf in BusinessLogic.businessLogic.factData.allFacts)
                        {
                            if (sf.tag == st0)
                            {
                                found = true;
                                break;
                            }
                        }

                        if (!found)
                        {
                            fact = new SingleFact(company, intYear, Period, st0, decimal.MinValue, BusinessLogic.businessLogic.FactValueMissing);
                            BusinessLogic.businessLogic.factData.allFacts.Add(fact);
                        }
                    }
                    #endregion

                    #region Build the fact result
                    foreach (SingleFact sf in BusinessLogic.businessLogic.factData.allFacts)
                    {
                        try
                        {
                            if (sf.tag.tid== TID && sf.tag.isVisible && sf.tag.tid < BusinessLogic.businessLogic.tagsProcessor.reservedTID0)
                            {
                                if (sf.tag != null && sf.tag.tags != null && sf.tag.tags.Count > 0)
                                {
                                    // If this is the main tag, always overwrite
                                    if (sf.actualSelectedTag == sf.tag.tags[0].tagName)
                                    {
                                        if (sf.strValue == "")
                                            result[0, 0] = (double)sf.factValue;
                                        else if (result[0, 0] == null || (result[0, 0]).ToString() == BusinessLogic.businessLogic.FactValueMissing)
                                            result[0, 0] = sf.strValue;
                                        result[0, 1] = sf.strUnits;
                                    }
                                    else
                                    {
                                        if (result[0, 0] == null || (result[0, 0]).ToString() == BusinessLogic.businessLogic.FactValueMissing)
                                        {
                                            if (sf.strValue == "")
                                                result[0, 0] = (double)sf.factValue;
                                            else
                                                result[0, 0] = sf.strValue;
                                            result[0, 1] = sf.strUnits;
                                        }
                                    }
                                    result[0, 2] = sf.tag.visualName;
                                }

                                if (sf.tag.formulaTagDependencies != null &&
                                    (result[0, 0] == null || (result[0, 0]).ToString() == BusinessLogic.businessLogic.FactValueMissing))
                                {
                                    textFormula = sf.tag.formula;
                                    cellFormula = "=" + textFormula;
                                    // Find the value we need for each tag
                                    foreach (SingleTag st2 in sf.tag.formulaTagDependencies)
                                    {
                                        // Find what value corresponds to this tag
                                        foreach (SingleFact sf1 in BusinessLogic.businessLogic.factData.allFacts)
                                        {
                                            if (st2 != sf1.tag || sf.company.companyName != sf1.company.companyName
                                                || sf.year != sf1.year || sf.period != sf1.period)
                                                continue;
                                            // Correct order is important
                                            if (sf1.actualSelectedTag != null)
                                            {
                                                found = false;
                                                for (int i = 0; i < st2.tags.Count; i++)
                                                {
                                                    if (sf1.actualSelectedTag == st2.tags[i].tagName)
                                                    {
                                                        found = true;
                                                        break;
                                                    }
                                                }
                                            }
                                            else
                                                found = true;
                                            if (found)
                                            {
                                                foreach (BasicTag bt in sf1.tag.tags)
                                                {
                                                    if (sf1.strValue == "")
                                                        cellFormula = cellFormula.Replace(String.Format("[{0}]", bt.tagName), sf1.factValue.ToString());
                                                    else
                                                        cellFormula = cellFormula.Replace(String.Format("[{0}]", bt.tagName), sf1.strValue);
                                                }
                                                if (sf1.strValue == "")
                                                    cellFormula = cellFormula.Replace(String.Format("[{0}]", sf1.tag.tid), sf1.factValue.ToString());
                                                else
                                                    cellFormula = cellFormula.Replace(String.Format("[{0}]", sf1.tag.tid), sf1.strValue);
                                            }
                                        }
                                    }
                                    cellFormula = Regex.Replace(cellFormula, "[[A-Za-z0-9]+]", BusinessLogic.businessLogic.FactValueMissing);
                                    cellFormula = cellFormula.Replace("[", "");
                                    cellFormula = cellFormula.Replace("]", "");

                                    //Microsoft.Office.Interop.Excel.Application xlApp = new Microsoft.Office.Interop.Excel.Application();
                                    Microsoft.Office.Interop.Excel.Application xlApp = (Microsoft.Office.Interop.Excel.Application)ExcelDna.Integration.ExcelDnaUtil.Application;
                                    result[0, 0] = xlApp.Evaluate((object)cellFormula);
                                    result[0, 1] = "Formula";
                                    result[0, 2] = sf.tag.visualName;
                                    //cell.NoteText(sf.tag.noteText);
                                }
                            }
                        }
                        catch (Exception)
                        {
                            return objNaN;
                        }
                    }

                    #endregion
                }
                else if (DBRecord.Count > 0 && Member.ToLower() == "all")
                {
                    result = new object[DBRecord.Count + 1, 2];
                    result[0, 0] = "Count: " + DBRecord.Count.ToString();
                    result[0, 1] = "Segment Name";
                    for (int inx = 0; inx < DBRecord.Count; inx++)
                    {
                        result[inx + 1, 0] = DBRecord[inx][0];
                        result[inx + 1, 1] = DBRecord[inx][1];
                    }
                }
                else if (DBRecord.Count > 0 && (Member == "" || DomainSegment == Member))
                    result = new object[1, 1] { { DBRecord[DBRecord.Count - 1][0] } };
                else
                    result = new object[1, 1] { { NoData } };

                return result;
            }
            return objNaN;
        }
        #endregion
    }
}