/* 
 * XBRLAnalyst
 * https://bitbucket.org/FinDynamics/xbrlanalyst
 * 
 * Copyright 2013, FinDynamics Inc.
 * Licensed under the GPL license:
 * http://www.gnu.org/licenses/gpl.html
 *
 *
 *
 * This software is provided 'as-is', without any expressed or implied
 * warranty. In no event will the authors be held liable for any damages arising from the use of this software.
 *
 */
using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace XBRLAnalyst
{
    public class CompanyEntity
    {
        public String entity_code;
        public int entity_id;
        public String companyName;
        public String ticker;
        public int position = -1;

        public CompanyEntity(String entity_code, int entity_id, String companyName, String ticker)
        {
            this.entity_code = entity_code;
            this.entity_id = entity_id;
            this.companyName = companyName;
            this.ticker = ticker;
        }
        public override String ToString()
        {
            return String.Format("{0} - {1}", entity_code, companyName);
        }
    }

    public class IndustryCode
    {
        public int industryCode;
        public String industryName;
        public List<CompanyEntity> companiesList = new List<CompanyEntity>();
        public IndustryCode(int industryCode, String industryName)
        {
            this.industryCode = industryCode;
            this.industryName = industryName;
        }
        public override String ToString()
        {
            return String.Format("{0} - {1}", industryCode, industryName);
        }
    }

    public class DataMiningClass
    {
        public List<IndustryCode> allIndustries = new List<IndustryCode>();
        public Dictionary<int, CompanyEntity> allCompanies = new Dictionary<int, CompanyEntity>();

        public void FillUpEntityLists()
        {
            CompanyEntity company = null;
            IndustryCode industry = null;

            String qry;
            String entity_code = null;
            String entity_name = null;
            String industryDescription = null;
            int sicCode = 0;
            int entity_id = 0;
            String ticker = null;

            int lastSicCode = -1;
            String lastEntityCode = null;

            qry = "SELECT DISTINCT entity_code, entity.entity_id, entity.entity_name, standard_industrial_classification, description, UPPER(SUBSTRING(entry_url from 'http://www.sec.gov/Archives/edgar/data/[0-9]+/[0-9]+/([a-z]+)-[0-9]+\\.xml')) AS ticker FROM entity, accession, sic_code WHERE sic_code.sic_code_id = accession.standard_industrial_classification AND entity.entity_id = accession.entity_id ORDER BY description, entity_name";
            NpgsqlDataReader reader = null;
            try
            {
                NpgsqlCommand command = new NpgsqlCommand(qry, BusinessLogic.businessLogic.dbConnection);
                reader = command.ExecuteReader();
            }
            catch (Exception)
            { return; }

            // Prepare the first row, first company, industry and filing
            // Without it, we do not have any parent reference
            if (reader.Read())
            {
                try
                {
                    entity_code = reader.GetString(0);
                }
                catch (Exception)
                {
                    entity_code = null;
                }

                try
                {
                    entity_id = reader.GetInt32(1);
                }
                catch (Exception)
                {
                    entity_id = -1;
                }

                try
                {
                    entity_name = reader.GetString(2);
                }
                catch (Exception)
                {
                    entity_name = null;
                }
                try
                {
                    sicCode = reader.GetInt32(3);
                }
                catch (Exception)
                {
                    sicCode = -1;
                }
                try
                {
                    industryDescription = reader.GetString(4);
                }
                catch (Exception)
                {
                    industryDescription = null;
                }

                try
                {
                    ticker = reader.GetString(5);
                }
                catch (Exception)
                {
                    ticker = null;
                }

                // Create an industry
                industry = new IndustryCode(sicCode, industryDescription);
                allIndustries.Add(industry);
                lastSicCode = sicCode;

                // Create a company
                company = new CompanyEntity(entity_code, entity_id, entity_name, ticker);

                allCompanies.Add(entity_id, company);
                industry.companiesList.Add(company);
                lastEntityCode = entity_code;
            }


            while (reader.Read())
            {
                try
                {
                    entity_code = reader.GetString(0);
                    entity_id = reader.GetInt32(1);
                    entity_name = reader.GetString(2);
                    sicCode = reader.GetInt32(3);
                    industryDescription = reader.GetString(4);
                    ticker = reader.GetString(5);
                }
                catch (Exception)
                {

                }

                // Check if we need to create another industry entry
                if (sicCode != lastSicCode)
                {
                    // Create an industry
                    industry = new IndustryCode(sicCode, industryDescription);
                    allIndustries.Add(industry);
                    lastSicCode = sicCode;
                }

                // Check if we need to create another company entry
                if (entity_code != lastEntityCode)
                {
                    // Create a company
                    company = new CompanyEntity(entity_code, entity_id, entity_name, ticker);
                    CompanyEntity anotherCompany;
                    if (allCompanies.TryGetValue(entity_id, out anotherCompany))
                        industry.companiesList.Add(anotherCompany);
                    else
                    {
                        industry.companiesList.Add(company);
                        allCompanies.Add(entity_id, company);
                    }
                    lastEntityCode = entity_code;
                }
            }
            reader.Close();
        }
    }
}
